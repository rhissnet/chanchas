<?php

return [

    'status' => [
        0 => 'Inactivo',
        1 => 'Activo'
    ],

    'weekdays' =>[
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado'            
    ],

    'bet_status' => [
        1 => 'Activo',
        2 => 'Pagado',
        3 => 'Anulada'
    ],



];
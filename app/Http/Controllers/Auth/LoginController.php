<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cms/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request,$user)
    {
        return redirect()->route('dashboard')->withCookie(cookie()->forever('allowCkfinder', "1"));   
    }


    public function login(Request $request)
    {   

        $input = $request->all();


        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        
  
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

       
        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'],'active'=>1)))
        {
            return redirect()->route('dashboard')->withCookie(cookie()->forever('allowCkfinder', "1"));
           
        }else{
            return redirect()->route('login')->with('error','Usuario y contraseña pueden tener un error.');
        }

          

    }

    protected function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $cookie = cookie()->forget('allowCkfinder');

        return redirect('/')->withCookie($cookie);
    }
}

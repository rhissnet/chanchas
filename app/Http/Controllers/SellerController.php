<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class SellerController extends Controller
{

    /**
     * Undocumented function
     *
     * @return void
     */
    public function index()
    {
        return view('admin.sellers.list');
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function jsonList()
    {

        $user = \Auth::user();

        if ($user->hasrole(['admin', 'manager'])) {

            $sellers = User::whereHas('roles', function ($query) {
                $query->where('id', '!=', '1')->where('id', '!=', '2');
            })->leftJoin('profiles as p', 'p.user_id', 'users.id')
                           ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                           ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                           ->select('users.*', 'roles.public_name as rol', 'p.code as code');

        }


        if ($user->hasrole(['supervisor'])) {

            $sellers = User::whereHas('roles', function ($query) {
                $query->where('id', '!=', '1')->where('id', '!=', '2');
            })->leftJoin('profiles as p', 'p.user_id', 'users.id')
                           ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                           ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
                           ->where('p.superior_id', $user->id)
                           ->select('users.*', 'roles.public_name as rol', 'p.code as code');

        }


        return DataTables::of($sellers)
                            ->editColumn('user', function ($seller) {
                                return $seller->username;;
                            })
                         ->editColumn('rol', function ($seller) {
                             return $seller->rol . (!empty($seller->code) ? ' <span class="badge bg-main">código: '. $seller->code .'</span>' : '');
                         })
                         ->addColumn('active', function ($service) {
                            return ($service->active == 1)? 'Activo':'Inactico' ;
                        })
                         ->addColumn('sales', function ($service) {
                             return Bet::where('user_id', $service->id)->count('id');
                         })
                         ->addColumn('options', function ($service) {
                             //<a class="btn btn-sm btn-outline-main" href="' . route('Service',['id' => $service->id]) . '" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> Ver</a>
                             return '
            <a class="btn btn-sm btn-outline-main" href="' . route('sellers.edit',
                                     ['id' => $service->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            <a class="btn btn-sm btn-outline  btn-outline-danger" href="javascript:deleteWithPassword(' . $service->id . ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>';
                         })
                         ->rawColumns(['options','rol'])
                         ->make();
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     *
     * @return void
     */
    public function edit($id)
    {
        $reg = array();

        $user        = \Auth::user();
        $actual_role = $user->getRoleNames();

        $reg['message']      = session()->get('message');
        $reg['type_message'] = session()->get('type_message');

        $idrole      = Role::where('name', $actual_role)->select('id')->first();
        $reg['rols'] = Role::where('id', '>', $idrole->id)->pluck('public_name', 'id');

        if ($id > 0) {

            $reg['reg']     = User::find($id);
            $reg['role_id'] = $reg['reg']->roles()->first()->id;
            $reg['pr']      = Profile::where('user_id', $id)->first();

        }

        $reg['id'] = $id;

        return view('admin.sellers.edit', $reg);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @param Request $request
     *
     * @return void
     */
    public function updateOrCreate($id = 0, Request $request)
    {
        $data      = $request->usr;
        $data_prof = $request->prof;

        /*if ( ! empty($id) && Profile::has('user')->where([
                ['user_id', '!=', $id],
                ['code', $data_prof['code']]
            ])->exists()) {
            \Session::flash('type_message', 'error');
            \Session::flash('message', 'El código del vendedor ya existe');

            return back();
        }*/

        $user_parent = \Auth::user();
        $role_id     = $request->role_id;
        $role = Role::find($role_id);

        $redirect    = 'sellers.edit';

        $data_prof['name']   = $data['name'];
        $data_prof['email']  = $data['email'];
        $data_prof['active'] = $data['active'];

        $data_prof['superior_id'] = $user_parent->roles->first()->id;

       
        if ($id > 0) {
            if (\Auth::Check()) {

                $user    = User::find($id);
                $profile = Profile::firstOrCreate(['user_id' => $id]);
                $profile->update($data_prof);

                if($user->username != $request->username){
                    $validated = $request->validate([
                        'username'  => 'required|unique:users|max:255',
                    ]);
            
                    $data['username'] = $request->username;
                }
                

                if (isset($request->password)) {
                  
                        $user->password = Hash::make($request->password);
                        $user->name     = isset($data['name']) ? $data['name'] : $user->email;
                        $user->email    = isset($data['email']) ? $data['email'] : $user->email;
                        $user->save();

                        \Session::flash('message', 'User has been saved successfully !');
                  

                    return redirect()->route($redirect, ['id' => $id]);

                } elseif ($user_parent->hasRole('Manager')) {
                    if ($role_id) {
                        $role     = Role::find($role_id);
                        $old_role = $user->getRoleNames();

                        if (count($old_role) > 0) {
                            $user->removeRole($old_role[0]);
                        }

                        $user->assignRole($role->name);
                    }

                    if (isset($request->password)) {
                        $user->password = Hash::make($request->password);
                    }

                    $user->name   = isset($data['name']) ? $data['name'] : $user_parent->email;
                    $user->status = $data['status'];

                    if (isset($data['email']) and $data['email'] != $user->email) {
                        $user->email = $data['email'];
                    }

                    $user->save();

                    \Session::flash('message', 'User has been saved successfully !');

                    return redirect()->route($redirect, ['id' => $id]);
                } else {

                    $data['password'] = $user->password;
                    $user->update($data);
                    \Session::flash('message', 'User has been saved successfully !');

                    return redirect()->route($redirect, ['id' => $id]);
                }
            }
        } else {

            $validated = $request->validate([
                'username'  => 'required|unique:users|max:255',
            ]);

            $data['username'] = $request->username;
            

            $user = User::create($data);
            $role = Role::find($role_id);

            $data_prof['user_id'] = $user->id;
            Profile::create($data_prof);

            $user->assignRole($role->name);

            $user->password = Hash::make($request->password);
            $user->update();

            return redirect()->route($redirect,  ['id' => $user->id]);
        }

        session()->flash('message', 'Información guardada.');

        return redirect()->route('sellers.edit', ['id' => $id]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     *
     * @return void
     */
    public function delete($id)
    {
        $user = User::find($id);

        if ($user != null) {
            Profile::where('user_id', $id)->delete();
            User::where('id', $id)->delete();
        }

        echo 1;
    }



    public function getSeller($id){

        $user = User::find($id);
        return response()->json(['name' => $user->name]);

    }


    /**
     * 
     */

    public function deleteSeller(Request $request){


        $user = \Auth::user();


        $id = $request->seller;
        $pass = $request->pass;

       
        if ( Hash::check($pass,  $user->password) ) {
            $user = User::find($id);

            if ($user != null) {
                Profile::where('user_id', $id)->delete();
                User::where('id', $id)->delete();
            }
    
            echo 1;
        }else
            echo 0;
        


    }

}

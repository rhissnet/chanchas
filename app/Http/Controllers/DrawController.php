<?php

namespace App\Http\Controllers;

use App\Exports\WinnerExport;
use App\Models\BetNumber;
use App\Models\Draw;
use App\Models\Loterry;
use App\Models\Winner;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;

class DrawController extends Controller
{
    private $draw;
    private $winner;

    /**
     * DrawController constructor.
     *
     * @param Draw $draw
     * @param Winner $winner
     */
    public function __construct(Draw $draw, Winner $winner)
    {
        $this->draw   = $draw;
        $this->winner = $winner;
    }

    /**
     * Muestra la lista de sorteos en el CMS del rol:admin.
     * @return \Illuminate\Http\Response
     * Created by  <Rhiss.net>
     */
    public function index()
    {
        $reg['lottery'] = Loterry::where([
            'weekday' => now()->dayOfWeek,
            'status'  => '1'
        ])->pluck('name', 'id')->prepend('Seleccione', '');
        $reg['message'] = session('message');

        return view('admin.draw.list', $reg);
    }

    /**
     * Crea o actualiza la información de un sorteo.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * Created by  <Rhiss.net>
     */
    public function updateOrCreate($id = 0, Request $request)
    {
        $dat            = $request->dat;
        $dat['user_id'] = auth()->id();
        if ($id == 0) {
            $draw = $this->draw->create($dat);

            //=========
            $date          = now()->toDateString();
            $number        = $draw->number;
            $lottery_id    = $draw->lottery_id;
            $digits        = str_split($number);
            $number3digits = substr($number, -3);
            $number2digits = substr($number, -2);
            $number1digit  = substr($number, -1);
            //=========

            //========= 4 cifras en orden
            $winners_four_digits = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['number', $number],
                ['date', $date]
            ])->get();
            $this->winner->createWinners($draw->id, '4_digits', $winners_four_digits);
            //=========

            //========= 4 cifras en cualquier orden
            $four_any                = array_filter(array_map(function ($v) {
                return implode("", array_filter($v));
            }, permute($digits)));
            $winners_four_digits_any = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['date', $date],
                ['combinated', '1'],
                ['number', '!=', $number]
            ])->whereIn('number', $four_any)->get();
            $this->winner->createWinners($draw->id, '4_digits_any', $winners_four_digits_any);
            //=========

            //========= 3 cifras en orden
            $winners_three_digits = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['number', $number3digits],
                ['date', $date],
            ])->get();
            $this->winner->createWinners($draw->id, '3_digits', $winners_three_digits);
            //=========

            //========= 3 cifras en cualquier orden
            $three_any                = array_filter(array_map(function ($v) {
                return implode("", array_filter($v));
            }, permute(str_split($number3digits))));
            $winners_three_digits_any = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['date', $date],
                ['combinated', '1'],
                ['number', '!=', $number3digits]
            ])->whereIn('number', $three_any)->get();
            $this->winner->createWinners($draw->id, '3_digits_any', $winners_three_digits_any);
            //=========

            //========= 2 cifras en orden
            $winners_two_digits = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['number', $number2digits],
                ['date', $date],
            ])->get();
            $this->winner->createWinners($draw->id, '2_digits', $winners_two_digits);
            //=========

            //========= 1 cifra
            $winners_one_digit = BetNumber::where([
                ['lottery_id', $lottery_id],
                ['number', $number1digit],
                ['date', $date]
            ])->get();
            $this->winner->createWinners($draw->id, '1_digit', $winners_one_digit);
            //=========
        }

        session()->flash('message', 'Información guardada.');

        return redirect()->route('draws.list');
    }

    /**
     * Elimina la información de un sorteo de la bd.
     *
     * @param $id
     * Created by  <Rhiss.net>
     */
    public function delete($id)
    {
        $this->winner->where('draw_id', $id)->delete();
        echo $this->draw->find($id)->delete();
    }

    /**
     * Lista los sorteos en formato JSON para mostrar con Datatable.
     * @return mixed
     * @throws \Exception
     * Created by <Rhiss.net>
     */
    public function jsonList()
    {
        $draws = $this->draw->with('lottery')->withCount('winners');

        return DataTables::of($draws)
                         ->addColumn('lottery_name', function ($draw) {
                             return $draw->lottery->name ?? '';
                         })
                         ->addColumn('options', function ($draw) {
                             return '<a class="btn btn-sm btn-outline-main" href="' . route('draws.winners',
                                     ['id' => $draw->id]) . '"><i class="fa fa-star" aria-hidden="true"></i> Ganadores</a>
                        <a class="btn btn-sm btn-outline-danger" href="javascript:deleteRow(' . $draw->id . ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>';
                         })
                         ->editColumn('created_at', function ($draw) {

                             return format_date($draw->created_at);
                         })
                         ->rawColumns(['options'])
                         ->make();

    }


    /**
     * Muestra la lista de ganadores de un en el CMS del rol:admin.
     * @return \Illuminate\Http\Response
     * Created by  <Rhiss.net>
     */
    public function indexWinners($id)
    {
        $reg['draw'] = $this->draw->find($id);

        return view('admin.draw.winner.list', $reg);
    }

    /**
     * Lista los ganadores de un sorteo en formato JSON para mostrar con Datatable.
     * @return mixed
     * @throws \Exception
     * Created by <Rhiss.net>
     */
    public function jsonListWinners($id)
    {
        $user      = auth()->user();
        $allow_pay = $user->hasRole(['admin', 'supervisor']);
        $winners   = $this->winner->with('bet')->where(['draw_id' => $id]);

        return DataTables::of($winners)
                         ->editColumn('bet_id', function ($winner) {
                             return $winner->bet->code;
                         })->editColumn('winner_type', function ($winner) {
                             $types = [
                                 '4_digits'     => '4 dígitos',
                                 '4_digits_any' => '4 dígitos cualquier orden',
                                 '3_digits'     => '3 dígitos',
                                 '3_digits_any' => '3 dígitos cualquier orden',
                                 '2_digits'     => '2 dígitos',
                                 '1_digit'      => '1 dígito'
                             ];

                             return $types[$winner->winner_type];
                         })
                         ->addColumn('options', function ($winner) use ($allow_pay) {
                             $options = '';
                             //if ($allow_pay) { //TODO pendiente
                             // $options .= '<a class="btn btn-sm btn-outline-main" href=""><i class="fa fa-dollar" aria-hidden="true"></i> Pagar</a>';
                             //}
                             $options .= '<a class="btn btn-sm btn-outline-danger" href="javascript:deleteRow(' . $winner->id . ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>';

                             return $options;
                         })
                         ->editColumn('created_at', function ($winner) {

                             return format_date($winner->created_at);
                         })
                         ->rawColumns(['options'])
                         ->make();

    }

    /**
     * Exporta los ganadores en formato xlsx.
     * Created by  <Rhiss.net>
     */
    public function exportWinners($id, Request $request)
    {
        $filters = $request->all();

        return Excel::download(new WinnerExport($id, $filters), 'ganadores.xlsx');
    }
}

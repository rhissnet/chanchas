<?php

namespace App\Http\Controllers;

use App\Models\BetNumber;
use App\Models\Draw;
use App\Models\Loterry;
use App\Models\Winner;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PaymentController extends Controller
{
    /**
     * Muestra la lista de sorteos en el CMS del rol:admin.
     * @return \Illuminate\Http\Response
     * Created by  <Rhiss.net>
     */
    public function draws()
    {
        $reg['lottery'] = Loterry::where([
            'weekday' => now()->dayOfWeek,
            'status'  => '1'
        ])->pluck('name', 'id')->prepend('Seleccione', '');
        $reg['message'] = session('message');

        return view('admin.payment.draw', $reg);
    }

    /**
     * Crea o actualiza la información de un sorteo.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * Created by  <Rhiss.net>
     */
    public function updateOrCreateDraw($id = 0, Request $request)
    {
        $dat            = $request->dat;
        $dat['user_id'] = auth()->id();
        if ($id == 0) {
            $draw = Draw::create($dat);

            //=========
            $date          = now()->toDateString();
            $number        = $draw->number;
            $lottery_id    = $draw->lottery_id;
            $digits        = str_split($number);
            $number3digits = substr($number, -3);
            $number2digits = substr($number, -2);
            $number1digit  = substr($number, -1);
            //=========

            //========= 4 cifras en orden
            $winners_four_digits = BetNumber::where([
                'lottery_id' => $lottery_id,
                'number'     => $number,
                'date'       => $date
            ])->get();
            foreach ($winners_four_digits as $winner) {
                Winner::create([
                    'winner_type'   => '4_digits',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('4_digits', $winner->amount)
                ]);
            }
            //=========

            //========= 4 cifras en cualquier orden
            $regex_four_any          = '^(?:' . $digits[0] . '()|' . $digits[1] . '()|' . $digits[2] . '()|' . $digits[3] . '()){4}\1\2\3\4$';
            $winners_four_digits_any = BetNumber::where([
                'lottery_id' => $lottery_id,
                'date'       => $date
            ])->where('number', '!=', $number)->where('number', 'REGEXP', $regex_four_any)->get();
            foreach ($winners_four_digits_any as $winner) {
                Winner::create([
                    'winner_type'   => '4_digits_any',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('4_digits_any', $winner->amount)
                ]);
            }
            //=========

            //========= 3 cifras en orden
            $winners_three_digits = BetNumber::where([
                'lottery_id' => $lottery_id,
                'number'     => $number3digits,
                'date'       => $date
            ])->get();
            foreach ($winners_three_digits as $winner) {
                Winner::create([
                    'winner_type'   => '3_digits',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('3_digits', $winner->amount)
                ]);
            }
            //=========

            //========= 3 cifras en cualquier orden
            $regex_three_any          = '^(?:' . $digits[1] . '()|' . $digits[2] . '()|' . $digits[3] . '()){3}\1\2\3$';
            $winners_three_digits_any = BetNumber::where([
                'lottery_id' => $lottery_id,
                'date'       => $date
            ])->where('number', '!=', $number3digits)->where('number', 'REGEXP', $regex_three_any)->get();
            foreach ($winners_three_digits_any as $winner) {
                Winner::create([
                    'winner_type'   => '3_digits_any',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('3_digits_any', $winner->amount)
                ]);
            }
            //=========

            //========= 2 cifras en orden
            $winners_two_digits = BetNumber::where([
                'lottery_id' => $lottery_id,
                'number'     => $number2digits,
                'date'       => $date
            ])->get();
            foreach ($winners_two_digits as $winner) {
                Winner::create([
                    'winner_type'   => '2_digits',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('2_digits', $winner->amount)
                ]);
            }
            //=========

            //========= 1 cifra
            $winners_one_digit = BetNumber::where([
                'lottery_id' => $lottery_id,
                'number'     => $number1digit,
                'date'       => $date
            ])->get();
            foreach ($winners_one_digit as $winner) {
                Winner::create([
                    'winner_type'   => '1_digit',
                    'draw_id'       => $draw->id,
                    'bet_id'        => $winner->bet_id,
                    'lottery_id'    => $winner->lottery_id,
                    'number'        => $winner->number,
                    'amount'        => $winner->amount,
                    'earned_amount' => calculate_earned_amount('1_digit', $winner->amount)
                ]);
            }
            //=========
        }

        session()->flash('message', 'Información guardada.');

        return redirect()->route('payment.draws');
    }

    /**
     * Elimina la información de un sorteo de la bd.
     *
     * @param $id
     * Created by  <Rhiss.net>
     */
    public function delete($id)
    {
        Winner::where('draw_id', $id)->delete();
        echo Draw::find($id)->delete();
    }

    /**
     * Lista los sorteos en formato JSON para mostrar con Datatable.
     * @return mixed
     * @throws \Exception
     * Created by <Rhiss.net>
     */
    public function jsonListDraws()
    {
        $draws = Draw::withCount('winners')->with('lottery');

        return DataTables::of($draws)
                         ->addColumn('lottery_name', function ($draw) {
                             return $draw->lottery->name ?? '';
                         })
                         ->addColumn('options', function ($draw) {
                             return '<a class="btn btn-sm btn-outline-main" href=""><i class="fa fa-star" aria-hidden="true"></i> Ganadores</a>
                        <a class="btn btn-sm btn-outline-danger" href="javascript:deleteRow(' . $draw->id . ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>';
                         })
                         ->editColumn('created_at', function ($draw) {

                             return format_date($draw->created_at);
                         })
                         ->rawColumns(['options'])
                         ->make();

    }
}

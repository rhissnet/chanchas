<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\BetNumber;
use App\Models\Configsite;
use App\Models\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $reg = array(); 
        $user = \Auth::user();

        $reg['date_start'] = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 15, date("Y")));
        $reg['date_end']   = date('Y-m-d');

        $reg['hotnumber'] = BetNumber::leftJoin('loterries as l','l.id','bet_numbers.lottery_id')
                    ->where('date',date('Y-m-d'))->groupBy('lottery_id', 'number')
                    ->selectraw('l.name, number, sum(amount) as total')->orderBy('total','desc')->first();

        $reg['max_value'] = Configsite::getInfo('max_total_number');

        if($reg['max_value'] > 0 && $reg['hotnumber'] != null)
            $reg['porcentage'] = ($reg['hotnumber']->total * 100) /  $reg['max_value']; 
        else    
         $reg['porcentage'] = 0;


         $reg['hot_numbers'] = BetNumber::leftJoin('loterries as l','l.id','bet_numbers.lottery_id')
         ->where('date',date('Y-m-d'))->groupBy('lottery_id', 'number')
         ->selectRaw('l.name, number ,sum(amount) as sum')
         ->orderBy('sum','desc')->take(8)->get();


         $reg['hotsales'] = Bet::where('date',date('Y-m-d'))->leftJoin('Users as u','u.id','bets.user_id')
         ->selectRaw('u.name , count(bets.id) as sales')
         ->groupBy('bets.user_id')
         ->orderBy('sales','desc')->take(8)->get();

    
    
        if($user->hasRole(['admin','manager'])){
            $reg['salesman'] = User::whereHas('roles', function ($query) {
                $query->where('id','!=', '1')->where('id','!=', '2');
            })->pluck('name', 'id');
        }else{
            $reg['salesman'] = $user->id;
        }

        return view('admin.dashboard', $reg);
    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function sales(Request $request){

        $date_start = $request->get('date_start');
        $date_end   = $request->get('date_end');
        $salesman   = $request->get('salesman');


       $bets = Bet::where(function ($query) use ($salesman) {

            if($salesman != ''){
                $query->where('user_id',$salesman);
            }
        
      })->whereBetween('date',[$date_start , $date_end])       
       ->groupBy('date')->selectRaw('date, sum(total) as sum, COUNT(*) as sale')->get();

      
       $start    = new DateTime($date_start);
       $interval = new DateInterval('P1D');
       $end      = new DateTime($date_end);
       $period   = new DatePeriod($start, $interval, $end, DatePeriod::EXCLUDE_START_DATE);
     
       $num_clas    = 0;
       $value_total = 0;

       $infocomapny  = array();
       $money        = array();

       foreach ($bets as $gest) {
           $realdate = explode(' ',$gest['date']);
           $infocomapny[$realdate[0]]  = $gest['sale'];
           $num_clas                    = $num_clas + $gest['sale'];
           $value_total                 = $value_total + $gest['sum'];
           $money[$realdate[0]]        = $gest['sum'];
       }


       if (!array_key_exists( $date_start,$infocomapny)) {
           $infocomapny[$date_start] = 0;
           $money[$date_start] = 0;
       }

       foreach ($period as $date) {
           $fecha = $date->format('Y-m-d');
           if (!array_key_exists( $fecha,$infocomapny)) {
               $infocomapny[$fecha] = 0;
               $money[$fecha]       = 0;
           }
       }
       if (!array_key_exists( $date_end,$infocomapny)) {
           $infocomapny[$date_end] = 0;
           $money[$date_end]       = 0;
       }

   
       ksort($infocomapny);
       ksort($money);

       $json = array();
       foreach($infocomapny as $value)
       {
           $json['data'][] = intval($value);
       }

       foreach($money as $value)
       {
           $json['money'][] = intval($value);
       }


       $json['name']     = 'Ventas';
       $json['cantidad'] = $num_clas;
       $json['tiempo']   = price_format($value_total);
   
       return response()->json($json);

    }


    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function general(Request $request){

        $date_start = $request->get('date_start');
        $date_end   = $request->get('date_end');
        $salesman   = $request->get('salesman');

        $lotteries = BetNumber::leftJoin('loterries as l','l.id','bet_numbers.lottery_id')
        ->leftJoin('bets as b','b.id','bet_numbers.bet_id')
        ->whereBetween('bet_numbers.date',[$date_start , $date_end])->groupBy('bet_numbers.lottery_id')
        ->where(function ($query) use ($salesman) {

            if($salesman != ''){
                $query->where('b.user_id',$salesman);
            }
        
        })
        ->selectRaw('l.name, sum(amount) as sum, COUNT(*) as number_times')
        ->orderBy('number_times','desc')->get();

        $numbers = BetNumber::whereBetween('bet_numbers.date',[$date_start , $date_end])
                  ->leftJoin('bets as b','b.id','bet_numbers.bet_id')
                  ->where(function ($query) use ($salesman) {

                    if($salesman != ''){
                        $query->where('b.user_id',$salesman);
                    }
                
                })
        ->groupBy('number')
        ->selectRaw('number ,sum(amount) as sum, COUNT(*) as number_times')
        ->orderBy('number_times','desc')->take(20)->get();

        $html = '';
        foreach($lotteries as $out)
        {
           $html .= '<tr>
           <td>' . $out['name'] . '</td>    
           <td align="center">' . $out['number_times'] . '</td>
           <td align="center">' . price_format( $out['sum'] ) . '</td>
            </tr>';
        }    
        $json['lottery'] = $html;


        $html = '';
        foreach($numbers as $out)
        {
           $html .= '<tr>
           <td>' . $out['number'] . '</td>    
           <td align="center">' . $out['number_times'] . '</td>
           <td align="center">' . price_format( $out['sum'] ) . '</td>
            </tr>';
        }    
        $json['numbers'] = $html;
       

        return response()->json($json);
    }
}

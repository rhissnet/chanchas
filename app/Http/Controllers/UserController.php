<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    /**
     * Undocumented function
     *
     * @return void
     */
    public function index(){
        $reg = array();

        return view('admin.users.list',$reg);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function jsonList()
    {
        $users = User::select(['users.id', 'users.name', 'email', 'roles.public_name as role', 'users.created_at','users.active'])
            ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('users.id', '<>', 2)
            ->where('roles.id', '<>', 6);

        return \DataTables::of($users)
           
            ->addColumn('created', function ($user) {
                return format_date($user->created_at);
            })
            ->addColumn('active', function ($user) {
                return ($user->active == 1)?'Activo':'Inactivo';
            })
            ->addColumn('options', function ($user) {
                $options = '<a class="btn btn-outline btn-sm btn-primary" href="'.route('users.edit', ['id' => $user->id]).'"><i class="fa fa-edit"></i></a>
                            <a class="btn btn-outline btn-sm btn-danger" href="javascript:delete_row('.$user->id.')"><i class="fa fa-trash"></i></a>';

                return $options;
            })
            ->rawColumns(['options'])
            ->make(TRUE);
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function edit($id=0){
        $reg = array();

        $user         = \Auth::user();
        $actual_role = $user->getRoleNames();

        $reg['message']     = session()->get('message');

        $reg['rols'] = Role::pluck('public_name','id');
        $reg['role_id'] = 1;
    
        if($id > 0){

            $reg['reg'] = User::find($id);
            $reg['role_id'] = $reg['reg']->roles()->first()->id;  
            $reg['pr']  = Profile::where('user_id',$id)->first();

        }

        $reg['id'] = $id;
        
        return view('admin.users.edit',$reg);
    }


    /**
     * Undocumented function
     *
     * @param integer $id
     * @param Request $request
     * @return void
     */
    public function updateOrCreate($id = 0, Request $request){
        $dat  = $request->dat;
        $info = $request->info;

        $user_parent = Auth::user();
        $data        = $request->usr;
        $data_prof   = $request->prof;
        $role_id     = $request->role_id;
        $redirect    = 'users.edit';

        $data_prof['name']  = $data['name'];
        $data_prof['email'] = $data['email'];
        $data_prof['active'] = $data['active'];


        if ($id > 0)
        {
            if (Auth::Check())
            {

                $user = User::find($id);
                $profile = Profile::firstOrCreate(['user_id' => $id]);
                $profile->update($data_prof);
              

                if($user->username != $request->username){
                    $validated = $request->validate([
                        'username'  => 'required|unique:users|max:255',
                    ]);
            
                    $data['username'] = $request->username;
                }

                if (isset($data['current']))
                {
                    $current = $user->password;

                    if (Hash::check($data['current'], $current))
                    {
                        $user->password = Hash::make($data['password']);
                        $user->name     = isset($data['name']) ? $data['name'] : $user->email;
                        $user->email    = isset($data['username']) ? $data['username'] : $user->username;
                        $user->save();

                        \Session::flash('message', 'User has been saved successfully !');
                    } else
                        \Session::flash('error', 'The current password is incorrect');

                    return redirect()->route($redirect, ['id' => $id]);

                } elseif ($user_parent->hasRole('Manager'))
                {
                    if ($role_id)
                    {
                        $role     = Role::find($role_id);
                        $old_role = $user->getRoleNames();

                        if (count($old_role) > 0)
                            $user->removeRole($old_role[0]);

                        $user->assignRole($role->name);
                    }

                    if (isset($data['password']))
                        $user->password = Hash::make($data['password']);

                    $user->name   = isset($data['name']) ? $data['name'] : $user_parent->email;
                    $user->status = $data['status'];

                    if (isset($data['email']) and $data['email'] != $user->email)
                        $user->email = $data['email'];

                    $user->save();

                    \Session::flash('message', 'User has been saved successfully !');

                    return redirect()->route($redirect, ['id' => $id]);
                } else
                {
                    $data['password'] = $user->password;
                    $user->update($data);
                    \Session::flash('message', 'User has been saved successfully !');

                    return redirect()->route($redirect, ['id' => $id]);
                }
            }
        } else
        {

            $validated = $request->validate([
                'username'  => 'required|unique:users|max:255',
            ]);

            $data['username'] = $request->username;



            $user                 = User::create($data);
            $role                 = Role::find($role_id);
            $data_prof['user_id'] = $user->id;
            Profile::create($data_prof);

            $user->assignRole($role->name);

            $user->password = Hash::make($request->get('password'));
            $user->save();

            return redirect()->route('users.edit', ['id' => $user->id]);
        }

    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        return response()->json(User::find($id)->update(['status' => 0]));
    }
}

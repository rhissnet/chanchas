<?php

namespace App\Http\Controllers;

use App\Models\Bet;
use App\Models\Configsite;
use App\Models\Profile;
use App\Models\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    

    public function sales(){

        $day = date('d');

        if($day >= '16'){
            $reg['date_start'] = date('Y-m').'-16';
            $reg['date_end'] = date('Y-m-t');
        }else{
            $reg['date_start'] = date('Y-m').'-01';
            $reg['date_end'] = date('Y-m').'-15';
        }

        $reg['salesman'] = User::whereHas('roles', function ($query) {
            $query->where('id','!=', '1')->where('id','!=', '2');
        })->pluck('name', 'id');

       
        return view('admin.reports.sales',$reg);
    }



    public function salesGeneral(Request $request){
        $date_start = $request->get('date_start');
        $date_end   = $request->get('date_end');
        $salesman   = $request->get('salesman');


       $bets = Bet::where(function ($query) use ($salesman) {

        
      })->leftjoin('users as u','u.id','bets.user_id')->whereBetween('date',[$date_start , $date_end])       
       ->groupBy('user_id')->selectRaw('sum(total) as sum, COUNT(*) as sale, u.name,user_id')->get();

      
       $html = '';
       $html2 = '';
       $html3 = '';


       $value = Configsite::getInfo('salesman_commission');
       $base  = Configsite::getInfo('commission_base');
       $operacion =   $value / 100;
       $local_commission = Configsite::getInfo('local_commission');


       foreach($bets as $out)
       {
        
            $user    = User::find($out['user_id']);
            $profile = Profile::where('user_id',$out['user_id'])->first();

            $role = $user->getRoleNames()->first();
            $comision = 0;

            if($role == 'salesman'){    
                if($out['sum'] > $base){
                    $comision = $out['sum'] * $operacion;
                }
                $html .= '<tr>
                <td>' . $out['name'] . ' ( '. $profile->code.')</td>    
                <td>' . $out['sale'] . '</td>    
                <td align="center"> ' . price_format( $out['sum'] ) . '</td>
                <td align="center">' . price_format($comision). '</td>
                    </tr>';
            }

            if($role == 'local'){    
                $comision = $out['sum'] * ($local_commission/100);
                $html2 .= '<tr>
                <td>' . $out['name'] . ' ( '. $profile->code.')</td>    
                <td>' . $out['sale'] . '</td>    
                <td align="center">' .  price_format($out['sum']) . '</td>
                <td align="center">' . price_format( $comision ) . '</td>
                </tr>';
            }

            if($role != 'local' && $role != 'salesman'){
                $html3 .= '<tr>
                <td>' . $out['name'] . '</td>    
                <td>' . $out['sale'] . '</td>    
                <td align="center">' .  price_format($out['sum']) . '</td>
                </tr>';

            }
        }   

       $json['numbers'] = $html;  
       $json['lottery'] = $html2;
       $json['others']  = $html3;
      

       return response()->json($json);
    }

}

<?php

namespace App\Http\Controllers;

use App\Exports\BetExport;
use App\Models\Bet;
use App\Models\BetNumber;
use App\Models\BlockNumber;
use App\Models\Configsite;
use App\Models\ConsecutiveNumber;
use App\Models\Loterry;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Yajra\DataTables\Facades\DataTables;

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\EscposImage;

class BetController extends Controller
{

    public function __construct(Bet $bet)
    {
        $this->bet = $bet;

    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * Created by <Rhiss.net>
     */
    public function index(Request $request)
    {
        $reg['lottery']     = Loterry::where(['weekday' => now()->dayOfWeek, 'status' => '1'])->pluck('name', 'id');
        $reg['all_lottery'] = Loterry::pluck('name', 'id');
        $reg['users']       = User::has('profile')->pluck('name', 'id');
        $reg['filters']     = $request->has('filter') ? array_filter($request->all()) : [];
        $reg['date']        = date('Y-m-d');
        

        return view('admin.bet.list', $reg);
    }


    /**
     * @return mixed
     * @throws \Exception
     * Created by <Rhiss.net>
     */
    public function jsonList(Request $request)
    {
        $conditions = $request->has('filter') ? $request->all() : [];
        $user       = auth()->user();
        $rol        = $user->getRoleNames()->first();

        $bets = $this->bet->when(! empty($conditions['date']), function ($query) use ($conditions) {
            return $query->where('bets.date', $conditions['date']);
        })->when(! empty($conditions['user_id']), function ($query) use ($conditions) {
            return $query->where('bets.user_id', $conditions['user_id']);
        })->when(! empty($conditions['consecutive']), function ($query) use ($conditions) {
            return $query->where(function ($q2) use ($conditions) {
                $q2->where('bets.consecutive', $conditions['consecutive'])
                   ->orWhere('bets.consecutive', ltrim($conditions['consecutive'], 0));
            });
        })->when(! empty($conditions['number']), function ($query) use ($conditions) {
            return $query->whereHas('numbers', function ($q2) use ($conditions) {
                $q2->where('number', $conditions['number']);
            });
        })->when(! empty($conditions['lottery_id']), function ($query) use ($conditions) {
            return $query->whereHas('numbers', function ($q2) use ($conditions) {
                $q2->where('lottery_id', $conditions['lottery_id']);
            });
        })->when($rol !== 'admin', function ($query) use ($user) {
            return $query->where('user_id', $user->id);
        })->orderby('id', 'desc');

        return DataTables::of($bets)
                         ->addColumn('code', function ($bet) {

                             return $bet->code;
                         })
                         ->addColumn('date', function ($bet) {

                             return format_date($bet['date']);
                         })
                         ->addColumn('total', function ($bet) {

                             return price_format($bet['total']);
                         })
                         ->addColumn('status', function ($bet) {

                             return '';
                         })
                         ->addColumn('seller', function ($bet) {
                             $user = User::find($bet['user_id']);

                             return $user->name;
                         })
                         ->addColumn('options', function ($user) {
                             $options = '<a class="btn btn-sm btn-outline-main" href="' . route('bet.preview',
                                     ['id' => $user->id]) . '"><i class="fa fa-eye"></i> Ver</a>';

                             return $options;
                         })
                         ->rawColumns(['options'])
                         ->make(true);

    }

    /**
     * Undocumented function
     *
     * @param integer $id
     *
     * @return void
     */
    public function edit($id = 0)
    {
        $reg['lottery'] = Loterry::where([
            'weekday' => now()->dayOfWeek,
            'status'  => '1'
        ])->pluck('name', 'id');

        $d1 = Carbon::createFromFormat('Y-m-d H:i',date('Y-m-d H:i'));
      
        $close_time  = Configsite::getInfo('close_time');
        $d2 = Carbon::createFromFormat('Y-m-d H:i', date('Y-m-d').$close_time);

        if( ($d1->gt($d2)) == true){

            $tomorrow    = $d1->addDay();
            $tomorrowday = $tomorrow->dayOfWeek;
    
            $reg['date']    = $tomorrow->toDateString(); ;
            if($tomorrowday == 0)
               $reg['date']    = $tomorrow->addDay()->toDateString(); ;

        }else{
          
            $reg['date']    = date('Y-m-d');
        }
        
        $reg['option']  = 'Agregar';


        return view('admin.bet.edit', $reg);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * Created by <Rhiss.net>
     */
    public function preview($id)
    {
        $reg['reg']     = $this->bet->find($id);
        $reg['numbers'] = $reg['reg']->numbers()->with('lottery')->get();

        $user       = auth()->user();
        $reg['rol'] = $user->getRoleNames()->first();

        return view('admin.bet.preview', $reg);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function updatePrint(Request $request){

        $id = $request->id;
        $bet = Bet::find($id);
        $bet->update(['printed'=>1]);

        echo '1';

    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @param Request $request
     *
     * @return void
     */
    public function updateOrCreate($id = 0, Request $request)
    {
        $user = auth()->user();
        if ($id == 0) {

            $lottery_id = $request->bet['lottery_id'];
            $number     = $request->bet['number'];
            $amount     = $request->bet['ammount'];
            $combinated = $request->bet['combinated'];
         

            $dat                = $request->dat;
            $date               = $dat['date'];
            $dat['status']      = 1;
            $dat['user_id']     = $user->id;
            $dat['seller_code'] = $user->profile->code ?? '';

            //====== consecutive
            $consecutive        = ConsecutiveNumber::where(['user_id' => $user->id])->orderBy('consecutive',
                'desc')->first();
            $consecutive_number = $dat['consecutive'] = ($consecutive->consecutive ?? 0) + 1;
            //======

            $dat['printed'] = 0;
            $dat['hash']    = md5(date('Ymdhis'));
 
            $bet = Bet::create($dat);
            ConsecutiveNumber::create([
                'user_id'     => $user->id,
                'bet_id'      => $bet->id,
                'consecutive' => $consecutive_number
            ]);

            $id = $bet->id;


            foreach ($lottery_id as $key => $out) {


                $betn['bet_id']     = $bet->id;
                $betn['lottery_id'] = $lottery_id[$key];
                $betn['number']     = $number[$key];
                $betn['amount']     = $amount[$key];
                $betn['combinated'] = (strlen($number[$key]) < 3)? '0':$combinated[$key];
                $betn['date']       = $date;
                $betn['status']     = '0';

                $lot_id = $lottery_id[$key];

                $BN = BetNumber::create($betn);

            }

            Bet::where('id',$bet->id)->update(['loterry_id'=>$lot_id]);

            session()->flash('message', 'Información guardada.');

            //$this->printPdf($id);

            return redirect()->route('bet.preview', ['id' => $id]);

        }

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * Created by <Rhiss.net>
     */
    public function validateNumber(Request $request)
    {
        $lottery = $request->lottery_id;

        $lots = array();
        if(strlen($lottery)>2){
            
            $lotday = substr($lottery, 0, 1);
            $lots = Loterry::where('weekday',$lotday)->get();

        }
  
        $number             = $request->space1;
        $bet_value          = $request->real_value;
        $played_ammount     = $request->ammountplayed;
        $total_bet_number   =  $bet_value + $played_ammount; 
        $combinated         = $request->combianted ;

        if(empty($lots)){
            $total_ammount = BetNumber::where('date', $request->date)->where('lottery_id',
            $request->lottery_id)->where('number', $number)
            ->where(function ($query) use ($combinated) { 
                if($combinated == 1){
                    $query->where('combinated',1);
                }else{
                    $query->Where('combinated','0');
                }
            })->sum('amount');

            $block_number = BlockNumber::where('number', $number)->where('lottery_id', $request->lottery_id)->count();
        }else{
            $total_ammount = BetNumber::where('date', $request->date)
                ->where(function ($query) use ($lots,$combinated) {
                    foreach($lots as $info){
                        $query->orWhere('lottery_id', $info->id);
                    }

                    if($combinated == 1){
                        $query->where('combinated',1);
                    }else{
                        $query->Where('combinated','0');
                    }

                })                      
                ->sum('amount');

            $block_number = BlockNumber::where('number', $number)->where(function ($query) use ($lots,$combinated) {
                foreach($lots as $info){
                    $query->orWhere('lottery_id', $info->id);
                }
            })->count();                
        }

        if($request->combianted == 1){
            $max_bet_day_per_number = Configsite::getInfo('max_total_number_com');
            $max_per_number = Configsite::getInfo('max_bet_com');

            
            
        }else{
            $max_bet_day_per_number = Configsite::getInfo('max_total_number');
            $max_per_number = Configsite::getInfo('max_bet');
        }

       

        $reg = ['status' => 'success'];

        if (empty($total_bet_number)) {
            $reg['status'] = 'error';
            $reg['code']   = 'block_number';
            $reg['msg']    = 'El monto apostado es requerido';
        }
        if ($block_number > 0) {
            $reg['status'] = 'error';
            $reg['code']   = 'block_number';
            $reg['msg']    = 'El número se encuentra bloqueado por la lotería. Intente con otro número.';
        }
        if ($total_bet_number > $max_per_number) {
            $reg['status'] = 'error';
            $reg['code']   = 'max_per_number';
            $reg['msg']    = 'Se alcanzó el monto total permitido para el número. Intente con un valor menor o cambiando de número.';
        }
        if (($total_ammount + $total_bet_number) > $max_bet_day_per_number) {
            $reg['status'] = 'error';
            $reg['code']   = 'max_per_day';
            $reg['msg']    = 'Se alcanzó el monto total permitido para el número. Intente con un valor menor o cambiando de número.';
        }

        return response()->json($reg);

    }

    /**
     * Undocumented function
     *
     * @param Request $request
     *
     * @return void
     */
    public function getLottery(Request $request)
    {
        $close_time  = Configsite::getInfo('close_time');

    
        $info = $d1 = Carbon::createFromFormat('Y-m-d',$request->getdate);
        $d2 = Carbon::createFromFormat('Y-m-d H:i', date('Y-m-d').$close_time);

        
        if( ($d1->gt($d2)) == true){

            //$dayinNumber = $d1->dayOfWeek;
           // $tomorrow    = $d1->addDay()->dayOfWeek;

           // dd($d1->dayOfWeek,$info->addDay()->dayOfWeek,$request->getdate);
           
           // $dayinNumber = $tomorrow;
           // if($tomorrow == 0)
             //   $dayinNumber = 1;
             $dayinNumber = $d1->dayOfWeek;
           
        }else{
            $dayinNumber = $d1->dayOfWeek;
        }

        $lottery = Loterry::where('weekday', $dayinNumber)->where('status', '1')->get();

        $html = '';
        $name = '';
        $id_both = $dayinNumber;
        foreach ($lottery as $item) {

            $spacer = ($name == '')? '': ' - ';
            $id_both .= $item['id'];
            $name .= $spacer.$item['abv'];
            $html .= '<option value=' . $item['id'] . '>' . $item['name'] . '</option>';

        }

        $html .= '<option value=' .$id_both. '>' .$name. '</option>';

        return response()->json($html);

    }

    /**
     * Exporta las chanchas en formato xlsx.
     * Created by  <Rhiss.net>
     */
    public function export(Request $request)
    {
        $filters = $request->all();

        return Excel::download(new BetExport($filters), 'chanchas.xlsx');
    }

    /**
     * @param $id
     * @param Request $request
     *
     * @return mixed
     * Created by <Rhiss.net>
     */
    public function printPdf($id)
    {
        $bet             = $this->bet->find($id);
        $data            = $bet->toArray();
        $data['bet']     = $bet;
        $data['numbers'] = $bet->numbers;
        $name            = 'chancha_' . $id . '.pdf';

        return view('exports.pdf.bet',$data);
        
       /*  return PDF::loadView('exports.pdf.bet', $data,[], [
            'format' => 'A8'
          ])->stream($name);
          */
       
      
       //return view('pdf.viewer');*/


      /* $printJob = Printing::newPrintTask()
       ->printer($printerId)
       ->file('path_to_file.pdf')
       ->send();
   
         $printJob->id(); */
        
    }


    /**
     * Retorna abreviacion de la chancha(s)
     *
     * @param [type] $id
     * @return void
     */
    static public function getchancha($id){
        
        $length = strlen($id);

        if($length > 2){
            $day = substr($id, 0, 1);
            $list = Loterry::where('weekday',$day)->get();

        
            $chancha = '';
            foreach($list as $out){                
                $chancha .= ($chancha == '')? $out['abv']: ' - '.$out['abv'];    
            }
        }else{
           $lot =  Loterry::where('id',$id)->first();
           $chancha = $lot['abv'];
        }

        return $chancha;
    }

}

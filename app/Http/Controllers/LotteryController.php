<?php

namespace App\Http\Controllers;

use App\Models\BetNumber;
use App\Models\BlockNumber;
use App\Models\Loterry;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class LotteryController extends Controller
{
    
    public function __construct(Loterry $lottery)
    {
        $this->lottery = $lottery;

    }

    /**
     * Undocumented function
     *
     * @return void
     */    
    public function index(){

        return view('admin.lottery.list');
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function edit($id=0){

        $reg = array();
        $reg['option'] = 'Agregar';

        $reg['numbers'] = array();

        if($id > 0){
            $reg['reg'] = $this->lottery->find($id);
            $reg['option'] = 'Editar';

            $reg['numbers'] = BlockNumber::where('lottery_id',$id)->get();
        }

        $reg['message'] = session('message');

        return view('admin.lottery.edit',$reg);

    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function jsonList()
    {
        $lotteries = $this->lottery->datatable();

        return DataTables::of($lotteries)
                         ->addColumn('options', function ($article) {
                             return '
                        <a class="btn btn-sm btn-outline-main" href="' . route('lottery.edit',
                                     ['id' => $article->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
                        <a class="btn btn-sm btn-outline-danger" href="javascript:deleteRow(' . $article->id . ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a>';
                         })
                         ->editColumn('weekday', function ($article) {
                         
                            $weekday = config('arrays.weekdays');
                            return $weekday[$article->weekday];
                        })
                        ->editColumn('status', function ($article) {

                            $status = config('arrays.status');
                            return $status[$article->status];
                        })
                        ->editColumn('number', function ($article) {

                             return BlockNumber::where('lottery_id',$article->id)->count('id');
                         })
                         ->rawColumns(['options'])
                         ->make();
    }

    /**
     * Undocumented function
     *
     * @param integer $id
     * @param Request $request
     * @return void
     */
    public function updateOrCreate($id = 0, Request $request)
    {
        $dat  = $request->dat;
  
        if ($id > 0) {
            $lottery = $this->lottery->find($id); 
            $lottery->update($dat);
         
        } else {
            $lottery            = $this->lottery->create($dat);
            $id                 = $lottery->id;
        }
        session()->flash('message', 'Información guardada.');

        return redirect()->route('lottery.edit', ['id' => $id]);
    }
    
    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function delete($id)
    {
        echo  $this->lottery->find($id)->delete();
    }
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function saveblock(Request $request){

        $dat['lottery_id'] = $request->lottery_id;
        $dat['number']     = $request->number;
        $block = BlockNumber::create($dat);

        return response()->json($block);

    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function deleteblock($id){

        echo  BlockNumber::find($id)->delete();

    }
}
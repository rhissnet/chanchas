<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BetNumber extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    /**
     * Lotería asociada.
     * @return mixed
     * Created by  <Rhiss.net>
     */
    public function lottery()
    {
        return $this->belongsTo(Loterry::class, 'lottery_id')->withDefault();
    }
}

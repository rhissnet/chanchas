<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlockNumber extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Created by <Rhiss.net>
     */
    public function lottery()
    {
        return $this->belongsTo(Loterry::class, 'lottery_id');
    }
}

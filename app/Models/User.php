<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param string $token
     * Created by <Rhiss.net>
     */
    public function sendPasswordResetNotification($token)
    {
        $data          = $this->toArray();
        $data['token'] = $token;

        Mail::send('email.forgot', $data, function ($message) use ($data) {
            $admin = User::find(1)->name;
            $message->subject('Petición de cambio de contraseña desde ' . get_info('title_meta'));
            $message->from(get_info('email'), $admin);
            $message->to($data['email']);
        });
    }

    /**
     * 
     */

    public function role()
    {
        return $this->belongsTo(ModelHasRole::class, 'id', 'model_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Created by  <Rhiss.net>
     * User:       <Sebastián Álvarez Vargas>
     * Email:      <sebasalvarez@rhiss.net>
     * Date:       20/02/19
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class, 'id', 'user_id');
    }


    static public function checkemail($email){

        if($email != '') {
            if (self::Where('email', $email)->count() > 0)
                return False;
            else
                return True;
        }else
            return FALSE;

    }
}

<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bet extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    /**
     * Números de la chancha.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Created by <Rhiss.net>
     */
    public function numbers()
    {
        return $this->hasMany(BetNumber::class);
    }

    /**
     * @return string
     * Created by <Rhiss.net>
     */
    public function getCodeAttribute()
    {
         return $this->seller_code . ' '. format_consecutive($this->consecutive, 4);
    }


}

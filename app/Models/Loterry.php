<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loterry extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Created by <Rhiss.net>
     */
    public function blockNumbers()
    {
        return $this->hasMany(BlockNumber::class,'lottery_id');
    }

    /**
     * Undocumented function
     *
     * @param [type] $query
     * @return void
     */
    public function scopeDatatable($query)
    {
        return $query->select([
                         '*'
                     ]);

    }

}

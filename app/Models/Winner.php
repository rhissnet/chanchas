<?php

namespace App\Models;

use App\Traits\Storable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Winner extends Model
{
    use SoftDeletes;
    use Storable;

    protected $guarded = ['id'];

    /**
     * Chancha asociada.
     * @return mixed
     * Created by  <Rhiss.net>
     */
    public function bet()
    {
        return $this->belongsTo(Bet::class);
    }

    /**
     * Número asociado.
     * @return mixed
     * Created by  <Rhiss.net>
     */
    public function betNumber()
    {
        return $this->belongsTo(BetNumber::class);
    }

    /**
     * Lotería asociada.
     * @return mixed
     * Created by  <Rhiss.net>
     */
    public function lottery()
    {
        return $this->belongsTo(Loterry::class, 'lottery_id');
    }

    /**
     * Crea los ganadores de un sorteo.
     *
     * @param $draw_id
     * @param $type
     * @param $winners
     * Created by <Rhiss.net>
     */
    public function createWinners($draw_id, $type, $winners)
    {
        foreach ($winners as $winner) {
            $this->create([
                'winner_type'   => $type,
                'draw_id'       => $draw_id,
                'bet_id'        => $winner->bet_id,
                'bet_number_id' => $winner->id,
                'lottery_id'    => $winner->lottery_id,
                'number'        => $winner->number,
                'amount'        => $winner->amount,
                'earned_amount' => calculate_earned_amount($type, $winner->amount)
            ]);
        }
    }
}

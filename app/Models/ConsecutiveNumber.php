<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsecutiveNumber extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
     * Chancha asociada.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Created by <Rhiss.net>
     */
    public function bet()
    {
        return $this->belongsTo(Bet::class);
    }

    /**
     * Usuario asociado.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Created by <Rhiss.net>
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

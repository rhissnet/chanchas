<?php

namespace App\Console\Commands;

use App\Models\Bet;
use App\Models\BetNumber;
use App\Models\ConsecutiveNumber;
use App\Models\Loterry;
use App\Models\Profile;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Sitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:script';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //pobla las chanchas para ensatar
        $dates = ['2021-12-23', '2021-12-24'];
        $users = Profile::pluck('code', 'user_id')->toArray();
        for ($i = 0; $i < 1000; $i++) {
            $key  = array_rand($dates);
            $key2 = array_rand($users);
            $dat  = [
                'date'        => $dates[$key],
                'status'      => 1,
                'user_id'     => $key2,
                'seller_code' => $users[$key2],
            ];

            //====== consecutive
            $consecutive        = ConsecutiveNumber::where(['user_id' => $key2])->orderBy('consecutive',
                'desc')->first();
            $consecutive_number = $dat['consecutive'] = ($consecutive->consecutive ?? 0) + 1;
            //======

            $bet = Bet::create($dat);
            ConsecutiveNumber::create([
                'user_id'     => $key2,
                'bet_id'      => $bet->id,
                'consecutive' => $consecutive_number
            ]);

            $date2      = new Carbon($dates[$key]);
            $lottery_id = Loterry::where(['weekday' => $date2->dayOfWeek, 'status' => '1'])->pluck('name', 'id');
            $total      = 0;
            foreach ($lottery_id as $key2 => $out) {
                $betn['bet_id']     = $bet->id;
                $betn['lottery_id'] = $key2;
                $betn['number']     = mt_rand(0, 9999);
                $betn['amount']     = 5;
                $betn['date']       = $dates[$key];
                $betn['status']     = '0';

                BetNumber::create($betn);
                $total += $betn['amount'];
            }

            $bet->update(['total' => $total]);
        }

        $this->info('Finished script');
    }

}
<?php

namespace App\Exports;

use App\Models\Draw;
use App\Models\Winner;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WinnerExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use RegistersEventListeners;

    public function __construct($id, $filters)
    {
        $this->id      = $id;
        $this->filters = $filters;
    }

    public function headings(): array
    {
        $draw = Draw::find($this->id);
        $head = [
            ['Sotero: ' . $draw->lottery->name, 'Fecha: ' . $draw->created_at, 'Número: ' . $draw->number],
            [
                '# Chancha',
                'Tipo acierto',
                'Número',
                'Valor apostado',
                'Valor ganado',
            ]
        ];

        $filters = $this->filters;
        if ( ! empty($filters)) {
            $cells = $this->generateFiltersCells($filters);
            array_unshift($head, $cells[1]);
            array_unshift($head, $cells[0]);
        }

        return $head;
    }

    public function generateFiltersCells($filters)
    {
        $list   = [
            'date'   => 'Fecha',
            'number' => 'Número',
        ];
        $titles = ['Filtros:'];
        $values = [''];

        foreach ($list as $key => $item) {
            if ( ! isset($filters[$key])) {
                continue;
            }
            array_push($titles, "$item");
            array_push($values, $filters[$key]);
        }

        return [$titles, $values];
    }

    public function collection()
    {
        $conditions = $this->filters ?? [];
        $winners    = Winner::where(['draw_id' => $this->id])
                            ->join('bets', 'bets.id', 'winners.bet_id')
                            ->select([
                                DB::raw("CONCAT(bets.seller_code, bets.consecutive) as code"),
                                'winners.winner_type',
                                'winners.number',
                                'winners.number',
                                'winners.amount',
                                'winners.earned_amount',
                            ])->get();

        return $winners;
    }
}
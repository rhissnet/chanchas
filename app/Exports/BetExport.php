<?php

namespace App\Exports;

use App\Models\Bet;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BetExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use RegistersEventListeners;

    public function __construct($filters)
    {
        $this->filters = $filters;
    }

    public function headings(): array
    {
        $head = [
            [
                'Código vendedor',
                'Consecutivo',
                'Fecha',
                'Vendedor',
                'Números',
            ]
        ];

        $filters = $this->filters;
        if ( ! empty($filters)) {
            $cells = $this->generateFiltersCells($filters);
            array_unshift($head, $cells[1]);
            array_unshift($head, $cells[0]);
        }

        return $head;
    }

    public function generateFiltersCells($filters)
    {
        $list   = [
            'date'        => 'Fecha',
            'lottery_id'  => 'Lotería',
            'user_id'     => 'Vendedor',
            'consecutive' => 'Consecutivo',
            'number'      => 'Número',
        ];
        $titles = ['Filtros:'];
        $values = [''];

        foreach ($list as $key => $item) {
            if ( ! isset($filters[$key])) {
                continue;
            }
            array_push($titles, "$item");
            array_push($values, $filters[$key]);
        }

        return [$titles, $values];
    }

    public function collection()
    {
        $conditions = $this->filters;
        $user       = auth()->user();
        $rol        = $user->getRoleNames()->first();

        $bets = Bet::when(! empty($conditions['date']), function ($query) use ($conditions) {
            return $query->where('bets.date', $conditions['date']);
        })->when(! empty($conditions['user_id']), function ($query) use ($conditions) {
            return $query->where('bets.user_id', $conditions['user_id']);
        })->when(! empty($conditions['consecutive']), function ($query) use ($conditions) {
            return $query->where(function ($q2) use ($conditions) {
                $q2->where('bets.consecutive', $conditions['consecutive'])
                   ->orWhere('bets.consecutive', ltrim($conditions['consecutive'], 0));
            });
        })->when(! empty($conditions['number']), function ($query) use ($conditions) {
            return $query->whereHas('numbers', function ($q2) use ($conditions) {
                $q2->where('number', $conditions['number']);
            });
        })->when(! empty($conditions['lottery_id']), function ($query) use ($conditions) {
            return $query->whereHas('numbers', function ($q2) use ($conditions) {
                $q2->where('lottery_id', $conditions['lottery_id']);
            });
        })->when($rol !== 'admin', function ($query) use ($user) {
            return $query->where('user_id', $user->id);
        })->leftJoin('users', 'bets.user_id', 'users.id')
          ->leftJoin('bet_numbers', 'bet_numbers.bet_id', 'bets.id')
          ->orderby('bets.id', 'desc')->select([
            'bets.seller_code',
            'bets.consecutive',
            'bets.date',
            'users.name',
            DB::raw("CONCAT('Número: ', bet_numbers.number,' Monto: ',bet_numbers.amount) as bnumbers"),
        ])->get()->groupBy('id');

        return $bets;
    }
}
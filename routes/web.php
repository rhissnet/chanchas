<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('home', function () {
    return auth()->check() ? redirect()->route('dashboard') : redirect()->route('home');
});
Auth::routes();
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['minify']], function () {
    //Detail
    Route::get('contenido/{id}/{title?}', 'HomeController@detail')->name('Content');
    Route::get('servicio/{id}/{title?}', 'HomeController@detail')->name('Service');
    Route::get('articulo/{id}/{title?}', 'HomeController@detail')->name('Article');
    //Forms
    Route::get('contactenos', 'HomeController@contact')->name('contact');
});

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::group(['middleware' => ['auth', 'role:admin|manager|supervisor|salesman'], 'prefix' => 'cms'], function () {
    //Dashboard
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('general', 'DashboardController@general');
        Route::get('sales', 'DashboardController@sales');
        Route::get('general', 'DashboardController@general');
    });

    //Slide
    Route::group(['prefix' => 'slides'], function () {
        Route::get('edit/{id}', 'SlideController@edit')->name('slides.edit_slide');
        Route::post('edit/{id}', 'SlideController@update');
        Route::post('upload-image/{id?}', 'SlideController@uploadImage')->name('slides.upload');
        Route::post('edit-image/{id?}', 'SlideController@editImage')->name('slides.edit');
        Route::delete('delete-image/{id?}', 'SlideController@deleteImage')->name('slides.delete');
        Route::get('get-image/{id?}', 'SlideController@getImage')->name('slides.get');
        Route::post('order', 'SlideController@orderImages')->name('slides.order');
    });

    //Gallery
    Route::group(['prefix' => 'galleries'], function () {
        Route::post('upload-image/{id?}', 'GalleryController@uploadImage')->name('galleries.upload');
        Route::post('edit-image', 'GalleryController@editImage')->name('galleries.edit');
        Route::delete('delete-image/{id?}', 'GalleryController@deleteImage')->name('galleries.delete');
        Route::get('get-image/{id?}', 'GalleryController@getImage')->name('galleries.get');
        Route::post('order', 'GalleryController@orderImages')->name('galleries.order');
    });

    //Files
    Route::group(['prefix' => 'files'], function () {
        Route::post('upload-file/{id?}', 'FileController@uploadFile')->name('files.upload');
        Route::post('edit-file', 'FileController@editFile')->name('files.edit');
        Route::delete('delete-file/{id?}', 'FileController@deleteFile')->name('files.delete');
        Route::get('get-file/{id?}', 'FileController@getFile')->name('files.get');
        Route::post('order', 'FileController@orderFiles')->name('files.order');
    });

    //Video
    Route::group(['prefix' => 'videos'], function () {
        Route::any('upload-video/{id?}', 'VideoController@upload')->name('videos.upload');
        Route::post('edit-video', 'VideoController@edit')->name('videos.edit');
        Route::delete('delete-video/{id?}', 'VideoController@delete')->name('videos.delete');
        Route::get('get-video/{id?}', 'VideoController@get')->name('videos.get');
        Route::post('order', 'VideoController@order')->name('videos.order');
    });

    //Category
    Route::group(['prefix' => 'categories'], function () {
        Route::get('get/{id?}', 'CategoryController@get')->name('categories.get');
        Route::post('edit/{id?}', 'CategoryController@edit')->name('categories.edit');
        Route::get('list/{id?}/{type?}', 'CategoryController@index')->name('categories.list');
        Route::delete('delete/{id?}', 'CategoryController@delete')->name('categories.delete');
        Route::post('order', 'CategoryController@order')->name('categories.order');
        Route::post('upload-image', 'CategoryController@uploadImage')->name('categories.upload_image');
    });

    //Contents
    Route::group(['prefix' => 'contents'], function () {
        Route::get('list', 'ContentController@index')->name('contents.list');
        Route::get('json-list', 'ContentController@jsonList');
        Route::get('edit/{id}', 'ContentController@edit')->name('contents.edit');
        Route::post('edit/{id}', 'ContentController@updateOrCreate');
        Route::get('categories', 'ContentController@categories')->name('contents.categories');
        Route::delete('delete/{id}', 'ContentController@delete');
    });

    //Articles
    Route::group(['prefix' => 'articles'], function () {
        Route::get('list', 'ArticleController@index')->name('articles.list');
        Route::get('json-list', 'ArticleController@jsonList');
        Route::get('edit/{id}', 'ArticleController@edit')->name('articles.edit');
        Route::post('edit/{id}', 'ArticleController@updateOrCreate');
        Route::get('categories', 'ArticleController@categories')->name('articles.categories');
        Route::delete('delete/{id}', 'ArticleController@delete');
    });

    //Services
    Route::group(['prefix' => 'services'], function () {
        Route::get('list', 'ServiceController@index')->name('services.list');
        Route::get('json-list', 'ServiceController@jsonList');
        Route::get('edit/{id}', 'ServiceController@edit')->name('services.edit');
        Route::post('edit/{id}', 'ServiceController@updateOrCreate');
        Route::get('categories', 'ServiceController@categories')->name('services.categories');
        Route::delete('delete/{id}', 'ServiceController@delete');
        Route::post('order', 'ServiceController@order')->name('services.order');
        Route::get('get-all', 'ServiceController@get')->name('services.get_all');
    });

    //Locations
    Route::group(['prefix' => 'locations'], function () {
        Route::get('list', 'LocationController@index')->name('locations.list');
        Route::get('json-list', 'LocationController@jsonList');
        Route::get('edit/{id}', 'LocationController@edit')->name('locations.edit');
        Route::post('edit/{id}', 'LocationController@updateOrCreate');
        Route::delete('delete/{id}', 'LocationController@delete');
    });

    //Subscribes
    Route::group(['prefix' => 'subscribers'], function () {
        Route::get('list', 'SubscriberController@index')->name('subscribers.list');
        Route::get('json-list', 'SubscriberController@jsonList');
        Route::delete('delete/{id}', 'SubscriberController@delete');
        Route::get('export', 'SubscriberController@export')->name('subscribers.export');
    });

    //Configsite
    Route::group(['prefix' => 'configsite'], function () {
        Route::get('edit', 'ConfigsiteController@edit')->name('configsite.edit');
        Route::post('edit', 'ConfigsiteController@update');
    });

    //Account
    Route::group(['prefix' => 'account'], function () {
        Route::get('edit', 'AccountController@edit')->name('account.edit');
        Route::post('edit', 'AccountController@update');
    });

    //Users

    Route::group(['prefix' => 'users'], function () {
        Route::get('list', 'UserController@index')->name('users.list');
        Route::get('json-list', 'UserController@jsonList');
        Route::get('edit/{id}', 'UserController@edit')->name('users.edit');
        Route::post('edit/{id}', 'UserController@updateOrCreate');
        Route::post('delete/{id}', 'UserController@delete');
    });


    Route::group(['prefix' => 'lottery'], function () {
        Route::get('list', 'LotteryController@index')->name('lottery.list');
        Route::get('json-list', 'LotteryController@jsonList');
        Route::get('edit/{id}', 'LotteryController@edit')->name('lottery.edit');
        Route::post('edit/{id}', 'LotteryController@updateOrCreate');
        Route::post('delete/{id}', 'LotteryController@delete');

        Route::post('block-number', 'LotteryController@saveblock')->name('lottery.save_block');
        Route::get('delete-block-number/{id?}', 'LotteryController@deleteblock')->name('loterry.delete_number');
    });


    Route::group(['prefix' => 'bet'], function () {
        Route::get('list', 'BetController@index')->name('bet.list');
        Route::get('json-list', 'BetController@jsonList');
        Route::get('edit/{id}', 'BetController@edit')->name('bet.edit');
        Route::post('edit/{id}', 'BetController@updateOrCreate');
        Route::post('delete/{id}', 'BetController@delete');
        Route::get('export', 'BetController@export')->name('bet.export');

        Route::get('preview/{id}', 'BetController@preview')->name('bet.preview');

        Route::post('update_printed','BetController@updatePrint')->name('update_printed');
        Route::get('print/{id}', 'BetController@printPdf')->name('bet.print');


        Route::post('validate', 'BetController@validateNumber')->name('validate_number');
        Route::post('get-lottery', 'BetController@getLottery')->name('get_loterry');
    });


    Route::group(['prefix' => 'sellers'], function () {
        Route::get('list', 'SellerController@index')->name('sellers.list');
        Route::get('json-list', 'SellerController@jsonList');
        Route::get('edit/{id}', 'SellerController@edit')->name('sellers.edit');
        Route::post('edit/{id}', 'SellerController@updateOrCreate');
        Route::delete('delete/{id}', 'SellerController@delete');
        Route::get('get-seller/{id?}','SellerController@getSeller')->name('sellers.getName');
        Route::post('delete-seller','SellerController@deleteSeller')->name('sellers.deleteSeller');
    });

    Route::group(['prefix' => 'reports'], function () {
        Route::get('sellers', 'ReportController@sellers')->name('reports.sellers');

    });

    Route::group(['prefix' => 'draws'], function () {
        Route::get('list', 'DrawController@index')->name('draws.list');
        Route::get('json-list', 'DrawController@jsonList');
        Route::post('edit/{id}', 'DrawController@updateOrCreate')->name('draws.update');
        Route::delete('delete/{id}', 'DrawController@delete');

        //Winners
        Route::get('winners/list/{id}', 'DrawController@indexWinners')->name('draws.winners');
        Route::get('winners/json-list/{id}', 'DrawController@jsonListWinners')->name('draws.json_winners');
        Route::delete('winners/delete/{id?}', 'DrawController@deleteWinner')->name('draws.delete_winner');
        Route::get('winners/export/{id}', 'DrawController@exportWinners')->name('draws.winners.export');
    });

    Route::group(['prefix' => 'payments'], function () {
        Route::get('pay', 'PaymentController@payment')->name('payment.pay');
    });


    Route::group(['prefix' => 'reports'], function () {
        Route::get('ventas', 'ReportsController@sales')->name('reports.sale');
        Route::get('sales-general', 'ReportsController@salesGeneral');

       
    });

});

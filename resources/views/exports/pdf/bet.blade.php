<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <style type="text/css">
    
            @page { header: page-header ; margin-top:90px; margin-left:10px;margin-right:10px;margin-bottom:1px; }

            body {
                font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                color: rgba(0, 0, 0, 0.87);
                background-color: transparent;
                
            }

            .panel .table {
                border-color: #e7e8ea !important;
                }

                .table > tbody > tr > td,
                .table > tfoot > tr > td,
                .table > tbody > tr > th,
                .table > tfoot > tr > th {
                padding: 12px 24px;
                border-top: 1px solid #e7e8ea;
                }

                .table > thead > tr > th {
                padding: 12px 24px;
                border-bottom: 1px solid #e7e8ea;
                }

                .table-bordered {
                border-color: #e7e8ea;
                }

                .table-bordered > tbody > tr > td,
                .table-bordered > tbody > tr > th {
                border-color: #e7e8ea;
                }

                .table-bordered > thead > tr > th {
                border-color: #e7e8ea;
                }

                .table-striped > tbody > tr:nth-child(odd) > td,
                .table-striped > tbody > tr:nth-child(odd) > th {
                background-color: #f6f6f7;
                }

                .table-striped > thead > th {
                background-color: #f6f6f7;
                border-right: 1px solid #e7e8ea;
                }

                .table-striped > thead > th:last-child {
                border-right: none;
                }

                [md-ink-ripple] {
                position: relative;
                }
    
            tfoot tr td {
                font-weight : bold;
                font-size   : x-small;
            }
    
            .gray {
                background-color : lightgray
            }
    
         
            #revise {
                position : fixed;
                top      : 3%;
                left     : 28%;
                opacity  : .4;
            }
    
            .text-italic {
                font-style : italic;
            }
            
            .text-right{
                text-align: right;
            }
           
            .font-size{
                font-size: 12px;
            }

            .font-size-2{
                font-size: 10px;
            }


            .font-size-3{
                font-size: 8px;
            }

            .font-size-4{
                font-size: 10px;
            }
    
            .text-center {
                text-align: center;
            }
            .p-5 {
                padding : 10px 5px;
            }
            .page_break { page-break-before: always; }
        </style>
    
</head>
<body >
<htmlpageheader name="page-header" class="text-center " width="100%">
    <table class="table table-striped table-bordered"  width="100%" style="display:block">
        <tbody>
            <tr>
                <td colspan="2" class="text-center font-size">APUESTAS INKASOL S.A.C<br/><span class="font-size-3">RUC 20604661049</span></td>
            </tr>
            <tr>
                <td  class="font-size-3"> # {{$bet->code}}</td>
                <td class="font-size-3" > {{format_date($bet->date)}}</td>
            </tr>
            <tr>
                <td colspan="2" class="text-left font-size-3" >Chancha: {{  App\Http\Controllers\BetController::getchancha($bet->loterry_id) }}</td>  
            </tr>
        </tbody>
    </table>
</htmlpageheader>
<htmlpagefooter ></htmlpagefooter>
<main id='test'>
    @php 
    
    $total = count($numbers) ; $pages = ceil($total / 4); $n=1;$block[$n] = '';$k=0; $partial[$n]=0; 

    foreach($numbers as $number){

            $k++;
            $length = strlen($number->number);

            $astrics = 4 - $length;

            $final_number = $number->number;
            for($i=1; $i<($astrics +1); $i++){
                $final_number = '*'.$final_number;
            }
            
            
            $block[$n].='<tr>
                <td class="text-center font-size-4" width="20%">'.$final_number.'</td>
                <td class="text-center font-size-4" width="20%"> '. (($length > 2 && $number->combinated == 0)? price_format($number->amount) :'*') .'</td>
                <td class="text-center font-size-4" width="20%"> '. (($length > 2 && $number->combinated == 1)? price_format($number->amount) :'*')  .'</td>
                <td class="text-center font-size-4" width="20%"> '. (($length == 2)? price_format($number->amount) :'*') .'</td>
                <td class="text-center font-size-4" width="20%">'. (($length == 1)? price_format($number->amount) :'*') .'</td>
            </tr>';

            $partial[$n] += $number->amount;

            if( $k%4==0){
                $n++;
                $block[$n] = '';
                $partial[$n] = 0;
            }
            

         }

    @endphp

    @for($i=1; $i<$pages+1;$i++)
        @php $z = $i; @endphp
  
    <table class="table table-striped table-bordered" width="100%">   
        <tbody>
           
            <tr>
                <td colspan="5" class="font-size-3">&nbsp;</td>
            </tr>
            <tr>
                <td class="text-center font-size-3" ><strong>Num </strong></td>
                <td class="text-center font-size-3" ><strong>Dir</strong></td>
                <td class="text-center font-size-3" ><strong>Com</strong></td>
                <td class="text-center font-size-3" ><strong>Dos</strong></td>
                <td class="text-center font-size-3" ><strong>Un</strong></td>
            </tr>
            {!! $block[$z] !!}
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" width="80%" class="text-right font-size"><strong>Total:</strong></td>
                <td colspan="2"  class="text-right font-size-4">{{price_format($partial[$z])}}</td>
            </tr>
            </tbody>
        </table>

        @if($i<$pages)
        <div class="page_break"></div>
        @endif
    @endfor
</main>


<script src="{{ asset('js/admin-global.min.js') }}" defer></script>
<a class="btn btn-sm btn-outline-main" onclick="printJS({printable: \'bet_form\', type: \'html\', header: \'PrintJS - Form Element Selection\'})">
                                        
    <i class="fa fa-print"></i> Imprimir</a>
</body>

</html>
@extends('admin.layouts.app')

@section('title', 'Escritorio')

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @hasanyrole('manager|admin')
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="list-title">Datos Generales para {{ format_date(date('Y-m-d')) }}</h2>
                            <div class="row row-xs" ng-controller="ChartCtrl">
                                <div class="col-sm-4">
                                  @if($hotnumber != null)
                                  <div class="panel panel-card">
                                    <div class="p">
                                        <h4>Numero más Vendido</h4>
                                      <h5>{{ $hotnumber->name }} / Numero {{ $hotnumber->number }}</h4>
                                    </div>
                                    <div class="panel-body text-center">
                                      <div class="m-v-lg">
                                        Valor vendido
                                        <div class="h2 text-success font-bold">{{  price_format($hotnumber->total) }}</div>
                                      </div>
                        
                                    </div>
                                    <div class="b-t b-light p">
                                      Limite Monto Maximo ({{ price_format($max_value) }})
                                      <div class="progress progress-striped progress-sm r m-v-sm">
                                        <div class="progress-bar info" style="width:{{ $porcentage }}%">{{ $porcentage }}%</div>
                                      </div>
                                    </div>
                                  </div>
                                  @endif
                                </div>
                                <div class="col-sm-4">
                                    <div class="panel panel-card">
                                        <div class="panel-heading b-b b-light">Numero Calientes en chanchas</div>
                                        <div class="list-group no-border no-radius">
                                          @foreach($hot_numbers as $out)  
                                            <div class="list-group-item">
                                                <span class="pull-right">{{ price_format($out->sum) }}</span>
                                                <strong>{{ $out->number }}</strong> - {{ $out->name }} 
                                            </div>
                                          @endforeach
                                        </div>
                                      </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="panel panel-card">
                                    
                                      <div class="panel panel-card">
                                        <div class="panel-heading b-b b-light">Vendedores más activos</div>
                                        <div class="list-group no-border no-radius">
                                          @foreach($hotsales as $out)  
                                            <div class="list-group-item">
                                                <span class="pull-right">{{ ($out->sales) }}</span>
                                                <strong>{{ $out->name }}</strong> 
                                            </div>
                                          @endforeach
                                        </div>
                                      </div>
                                    
                                  </div>
                                </div>    
                              </div>
                              
                        </div>
                    </div>

                    @endhasanyrole
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="list-title">Filtrar por fecha</h2>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Fecha Inicio</label>
                                        <input type="text" id="date_start" class="form-control date_from"
                                            value="{{$date_start ?? null}}" readonly/>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Fecha Final</label>
                                        <input type="text" id="date_end" class="form-control date_until"
                                            value="{{$date_end ?? null}}"
                                            readonly/>
                                    </div>
                                </div>
                                @hasrole('admin|manager')
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Vendedor</label>
                                        {{ Form::select('salesman',$salesman,'',array('class'=>"form-control", 'id'=>'salesman' , 'placeholder'=>'Todos los vendedores')) }}
                                    </div>
                                </div>
                                @else
                                    {{ Form::hidden('salesman',$salesman,array('class'=>"form-control", 'id'=>'salesman')) }}
                                @endrole
                                <div class="col-lg-4">
                                    <button class="btn btn-outline-main btn-mt-25 btn-load" onclick="loadChart()">
                                        Cargar Estadísticas
                                    </button>
                                </div>
                            
                                <div class="col-lg-12">
                                    <div id="stats">
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <ul class="stats-overview list-group list-group-horizontal">
                                        <li class="list-group-item">
                                                <span class="text-main">
                                                    <b>Ventas Totales</b>
                                                </span>
                                            <span class="value price_amount" id="visits">...</span>
                                        </li>
                                        <li class="list-group-item">
                                                <span class="text-main">
                                                    <b>Chanchas </b>
                                                </span>
                                            <span class="value" id="time">...</span>
                                        </li>
                                    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>           
                    <div class="row">
                        <div class="col-lg-6">
                            <h2 class="list-title">Ventas por Loteria</h2>
                            <div class="table-responsive" >
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Loteria</th>
                                            <th style="text-align:center">Number de Apuestas</th>
                                            <th style="text-align:center" >Valor de Ventas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="lottery_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h2 class="list-title">Numeros más Vendidos</h2>
                            <div class="table-responsive" >
                                <table class="table table-hover table-bordered" id="traffic_table">
                                    <thead>
                                        <tr>
                                            <th>Number</th>
                                            <th style="text-align:center">Veces Jugado</th>
                                            <th style="text-align:center" >Valor de Ventas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="number_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('vendors/highcharts/code/js/highcharts.js')}}"></script>
@endsection
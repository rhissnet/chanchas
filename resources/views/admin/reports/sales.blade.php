
@extends('admin.layouts.app')

@section('title', 'Escritorio')

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 class="list-title">Filtrar por fecha</h2>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Fecha Inicio</label>
                                        <input type="text" id="date_start" class="form-control date_from"
                                            value="{{$date_start ?? null}}" readonly/>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Fecha Final</label>
                                        <input type="text" id="date_end" class="form-control date_until"
                                            value="{{$date_end ?? null}}"
                                            readonly/>
                                    </div>
                                </div>
                                @hasrole('admin|manager')
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Vendedor</label>
                                        {{ Form::select('salesman',$salesman,'',array('class'=>"form-control", 'id'=>'salesman' , 'placeholder'=>'Todos los vendedores')) }}
                                    </div>
                                </div>
                                @else
                                    {{ Form::hidden('salesman',$salesman,array('class'=>"form-control", 'id'=>'salesman')) }}
                                @endrole
                                <div class="col-lg-4">
                                    <button class="btn btn-outline-main btn-mt-25 btn-load" onclick="loadSalesChart()">
                                        Cargar Estadísticas
                                    </button>
                                </div>
                            
                                <div class="col-lg-12">
                                    <div id="stats">
                                    </div>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <ul class="stats-overview list-group list-group-horizontal">
                                        <li class="list-group-item">
                                                <span class="text-main">
                                                    <b>Ventas Vendedores</b>
                                                </span>
                                            <span class="value price_amount" id="visits">...</span>
                                        </li>
                                        <li class="list-group-item">
                                                <span class="text-main">
                                                    <b>Chanchas </b>
                                                </span>
                                            <span class="value" id="time">...</span>
                                        </li>
                                    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>           
                    <div class="row">
                       

                        <div class="col-lg-12">
                            <h2 class="list-title">Vendedores</h2>
                            <div class="table-responsive" >
                                <table class="table table-hover table-bordered" id="sales_report_table">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th style="text-align:center">Chanchas</th>
                                            <th>Ventas</th>
                                            <th style="text-align:center" >Comision {{ App\Models\Configsite::getInfo('salesman_commission') }}%</th>
                                        </tr>
                                    </thead>
                                    <tbody id="number_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h2 class="list-title">Ventas Puntos Fijos</h2>
                            <div class="table-responsive" >
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Vendedor Codigo</th>
                                            <th style="text-align:center">Chanchas</th>
                                            <th style="text-align:center">Ventas</th>
                                            <th style="text-align:center" >Comisión {{ App\Models\Configsite::getInfo('local_commission') }}%</th>
                                        </tr>
                                    </thead>
                                    <tbody id="lottery_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                            <h2 class="list-title">Vendidas por otros Usuarios</h2>
                            <div class="table-responsive" >
                                <table class="table table-hover table-bordered" id="sales_report_table_others">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th style="text-align:center">Chanchas</th>
                                            <th>Ventas</th>
                                        </tr>
                                    </thead>
                                    <tbody id="other_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('vendors/highcharts/code/js/highcharts.js')}}"></script>
@endsection
<!-- aside -->
<aside id="aside" class="app-aside modal fade " role="menu">
    <div class="left">
        <div class="box bg-white">
            <div class="navbar md-whiteframe-z1 no-radius bg-main">
                <a class="navbar-brand">
                    <img src="{{asset('images/inkasol.png')}}" alt="." style="max-height: 40px;">
                    <span class="hidden-folded m-l inline text-white">{{ 'Inkasol' }}</span>
                </a>
            </div>
            <div class="box-row">
                <div class="box-cell scrollable hover">
                    <div class="box-inner">
                        <div class="p hidden-folded blue-50"
                             style="background-image:url('{{asset('images/bg.png')}}'); background-size:cover">
                            <a class="block m-t-sm" ui-toggle-class="hide, show" target="#nav, #account">
                                <span class="block font-bold">
                                @role('admin')
                                Administrador
                                @endrole
                                </span>
                                {{auth()->user()->email}}
                            </a>
                        </div>
                        <div id="nav">
                            <nav ui-nav>
                                <ul class="nav">


                                    <li class="nav-header hidden-folded">
                                        Módulos
                                    </li>

                                    <li class="{{check_item_active('main_li','dashboard')}}">
                                        <a href="{{route('dashboard')}}">
                                            <i class="icon mdi-action-dashboard i-20"></i>
                                            <span class="font-normal">Escritorio</span>
                                        </a>
                                    </li>
                                    @hasrole('admin|manager|supervisor')
                                    <li class="{{check_item_active('main_li','sellers')}}">
                                        <a><span class="pull-right text-muted">
                                                <i class="fa fa-caret-down"></i></span>
                                            <i class="icon mdi-action-assignment i-20"></i>
                                            <span class="font-normal">Vendedores</span>
                                        </a>
                                        <ul class="nav nav-sub">
                                            <li class="{{check_item_active('inner_li_add','sellers','edit')}}">
                                                <a href="{{route('sellers.edit',['id'=>0])}}">Agregar Vendor</a>
                                            </li>
                                            <li class="{{check_item_active('inner_li_list','sellers','list,edit')}}">
                                                <a href="{{route('sellers.list')}}">Lista de Vendedores</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endhasrole
                                    @hasrole('admin|manager')
                                    <li class="{{check_item_active('main_li','report')}}">
                                        <a><span class="pull-right text-muted">
                                                <i class="fa fa-caret-down"></i></span>
                                            <i class="icon mdi-action-trending-up i-20"></i>
                                            <span class="font-normal">Reportes</span>
                                        </a>
                                        <ul class="nav nav-sub">
                                            <li class="{{check_item_active('inner_li','articles','categories')}}">
                                                <a href="{{route('reports.sale')}}">Reporte Ventas</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="{{check_item_active('main_li','draws')}}">
                                        <a><span class="pull-right text-muted">
                                                <i class="fa fa-caret-down"></i></span>
                                            <i class="icon mdi-editor-attach-money i-20"></i>
                                            <span class="font-normal">Sorteos / Pagos</span>
                                        </a>
                                        <ul class="nav nav-sub">
                                            <li class="{{check_item_active('inner_li_add','draws','list')}}">
                                                <a href="{{route('draws.list')}}">Sorteos</a>
                                            </li>
                                        </ul>
                                        {{--<ul class="nav nav-sub">--}}
                                            {{--<li class="{{check_item_active('inner_li_add','articles','edit')}}">--}}
                                                {{--<a href="{{route('payment.pay')}}">Pagos</a>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    </li>
                                    @endhasrole
                                    <li class="{{check_item_active('main_li','bet')}}">
                                        <a><span class="pull-right text-muted">
                                                <i class="fa fa-caret-down"></i></span>
                                            <i class="icon mdi-action-grade i-20"></i>
                                            <span class="font-normal">Chanchas</span>
                                        </a>
                                        <ul class="nav nav-sub">
                                            <li class="{{check_item_active('inner_li_add','bet','edit')}}">
                                                <a href="{{route('bet.edit',['id'=>0])}}">Agregar Chancha</a>
                                            </li>
                                            <li class="{{check_item_active('inner_li_list','bet','list,edit,preview')}}">
                                                <a href="{{route('bet.list')}}">Lista de Chanchas</a>
                                            </li>
                                        </ul>
                                    </li>


                                    <li class="b-b b m-v-sm"></li>
                                    @hasrole('admin|manager')
                                    <li class="{{check_item_active('main_li','configsite,lottery')}}">
                                        <a><span class="pull-right text-muted">
                                                <i class="fa fa-caret-down"></i></span>
                                                <i class="icon mdi-action-settings i-20"></i>
                                            <span class="font-normal">Configuraciónes</span>
                                        </a>
                                        <ul class="nav nav-sub">
                                            @hasrole('admin')
                                            <li class="{{check_item_active('main_li','configsite')}}">
                                                <a href="{{route('configsite.edit')}}">Generales</a>
                                            </li>
                                            @endhasrole
                                            <li class="{{check_item_active('inner_li_list','lottery','list,edit')}}">
                                                <a href="{{route('lottery.list')}}">Loterias</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endhasrole
                                    @hasrole('admin')
                                    <li class="{{check_item_active('main_li','users')}}">
                                        <a md-ink-ripple href="{{route('users.list')}}">
                                            <i class="icon mdi-action-account-child i-20"></i>
                                            <span>Usuarios</span>
                                        </a>
                                    </li>
                                    @endhasrole
                                    <li class="{{check_item_active('main_li','account')}}">
                                        <a md-ink-ripple href="{{route('account.edit')}}">
                                            <i class="icon mdi-action-perm-contact-cal i-20"></i>
                                            <span>Mi cuenta</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- / aside -->
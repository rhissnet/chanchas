@extends('admin.layouts.app')

@section('title', ($option ?? 'Agregar') . ' Chancha')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('bet.list')}}">Chanchas</a></li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs cnav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#info" aria-controls="info" role="tab" data-toggle="tab">CHANCHA</a>
                    </li>
                </ul>
                <form class="validate-form bet-form" method="POST" enctype="multipart/form-data" action=""
                      id="form_bet">
                    @csrf

                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>
                                    <div class="col-lg-2 form-group hidden">
                                        <label>Fecha</label>
                                        <input type="hidden" name="dat[date]" id="bet_date">
                                        <input type="hidden" class="form-control date_from_bet" name="dat[date]"
                                               id="date_chancha" readonly value="{{$date ?? null}}" required/>
                                    </div>
                                    <div class="col-lg-4 form-group">
                                        <label>&nbsp; </label>
                                        <a href="#" onclick="openBetModal()"
                                           class="btn btn-outline-main btn-sm form-control addnumb"><i
                                                    class="fa fa-plus-circle"></i> Agregar Número</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>

                                    <div class="col-lg-4 form-group">
                                        <div class="list-group md-whiteframe-z0" id="list_numbers">

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>
                                    <div class="col-lg-4 form-group">


                                        <div class="list-group md-whiteframe-z0">
                                            <div class="list-group-item">

                                                <h3>Total <span
                                                            class="pull-right label label-lg bg-success price_amount"
                                                            id="total_text">0</span></h3>
                                                <input value="0" name="dat[total]" type="hidden" id="total_final">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <button type="submit" class="btn btn-main"><i class="fa fa-save"></i> Guardar</button>
                        <a href="{{route('bet.list')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        var validatenumerUrl = '{{ route('validate_number') }}';
        var get_loterry_url  = '{{ route('get_loterry') }}';
        var close_time       = '{{ get_info('close_time')}}';
        var min_bet          = '{{ get_info('min_bet')}}';
        var start            = 1;
        var realnumber       = 1;
    </script>
@endsection
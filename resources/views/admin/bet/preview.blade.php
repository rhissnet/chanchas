@extends('admin.layouts.app')

@section('extracss')
<style type="text/css">
    
    @page { header: page-header ; margin-top:10px; margin-left:10px;margin-right:10px;margin-bottom:1px; }

    #inside {
        font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
        -webkit-font-smoothing: antialiased;
        color: rgba(0, 0, 0, 0.87);
        background-color: transparent;
        
    }

    .panel .table {
        border-color: #e7e8ea !important;
        }

        .table > tbody > tr > td,
        .table > tfoot > tr > td,
        .table > tbody > tr > th,
        .table > tfoot > tr > th {
        padding: 5px 5px;
        border-top: 1px solid #e7e8ea;
        }

        .table > thead > tr > th {
        padding: 5px 5px;
        border-bottom: 1px solid #e7e8ea;
        }

        .table-bordered {
        border-color: #e7e8ea;
        }

        .table-bordered > tbody > tr > td,
        .table-bordered > tbody > tr > th {
        border-color: #e7e8ea;
        }

        .table-bordered > thead > tr > th {
        border-color: #e7e8ea;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f6f6f7;
        }

        .table-striped > thead > th {
        background-color: #f6f6f7;
        border-right: 1px solid #e7e8ea;
        }

        .table-striped > thead > th:last-child {
        border-right: none;
        }

        [md-ink-ripple] {
        position: relative;
        }

    tfoot tr td {
        font-weight : bold;
        font-size   : x-small;
    }

    .gray {
        background-color : lightgray
    }

 
    #revise {
        position : fixed;
        top      : 3%;
        left     : 28%;
        opacity  : .4;
    }

    .text-italic {
        font-style : italic;
    }
    
    .text-right{
        text-align: right;
    }
   
    .font-size{
        font-size: 40px;
    }

    .font-size-2{
        font-size: 30px;
    }


    .font-size-3{
        font-size: 28px;
    }

    .font-size-4{
        font-size: 30px;
    }

    .text-center {
        text-align: center;
    }
    .p-5 {
        padding : 10px 5px;
    }
    .page_break { page-break-before: always; }
</style>
@endsection

@section('title', 'Ver chancha')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('bet.list')}}">Chanchas</a></li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs cnav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#info" aria-controls="info" role="tab" data-toggle="tab">CHANCHA</a>
                    </li>
                </ul>
                <form class="validate-form bet-form" method="POST" enctype="multipart/form-data" action=""
                      id="form_bet">
                    @csrf

                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>
                                    <div class="col-lg-2 form-group">
                                        <label>#</label>
                                        <input type="text" class="form-control date_from_bet" value="{{$reg->code}}"
                                               readonly=""/>
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>Día jugado</label>
                                        <input type="text" class="form-control date_from_bet" value="{{$reg->date}}"
                                               disabled=""/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>

                                    <div class="col-lg-4 form-group" id="thisprint">
                                        <div class="list-group md-whiteframe-z0">
                                            @foreach($numbers as $number)
                                                <li class="list-group-item"><span
                                                            class="pull-right badge badge-lg bg-success">S/{{$number->amount}}</span>
                                                    <div class="block font-bold">{{$number->number}}</div>
                                                    {{$number->lottery->name}}
                                                </li>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>
                                    <div class="col-lg-4 form-group">
                                        <div class="list-group md-whiteframe-z0">
                                            <div class="list-group-item">
                                                <h3>Total <span
                                                            class="pull-right label label-lg bg-success">{{$reg->total}}</span></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @php    

                                $html ='<table class="table table-striped table-bordered" style="width:1024px">\
                                        <tbody>\
                                            <tr>\
                                                <td colspan="2" class="text-center font-size" style="font-size:64px">APUESTAS INKASOL S.A.C<br/>\
                                                    <span class="font-size-3" style="font-size:54px">LA CHANCHA MILLONARIA</span><br/>\
                                                    <span class="font-size-3" style="font-size:46px">RUC 20604661049</span></td>\
                                            </tr>\
                                            <tr>\
                                                <td colspan="2" class="text-left font-size-3" style="font-size:46px;">&nbsp;</td>  \
                                            </tr>\
                                            <tr>\
                                                <td  class="font-size-3" style="font-size:46px; width:500px;"> # '.$reg->code.'</td>\
                                                <td class="font-size-3"  style="font-size:46px;width:500px;"> '.format_date($reg->date).'</td>\
                                            </tr>\
                                            <tr>\
                                                <td colspan="2" class="font-size-3" style="font-size:42px; width:1000px;">Cod.: '.($reg->hash).'</td>\
                                            </tr>\
                                            <tr>\
                                                <td class="text-left font-size-3" style="font-size:42px;width:500px;">Chancha: '.App\Http\Controllers\BetController::getchancha($reg->loterry_id).'</td>\
                                                <td class="font-size-3"  style="font-size:42px;width:500px;"> Imp. '.date('Y-m-d H:i:s').'</td>\
                                            </tr>\
                                        </tbody>\
                                    </table>\
                                    <table class="table table-striped table-bordered" style="width:1024px">\
                                    <tbody>\
                                        <tr>\
                                            <td colspan="5" class="font-size-3" style="font-size:40px">&nbsp;</td>\
                                        </tr>\
                                        <tr>\
                                            <td class="text-center font-size" style="font-size:46px" ><strong>Num </strong></td>\
                                            <td class="text-center font-size" style="font-size:46px" ><strong>Dir</strong></td>\
                                            <td class="text-center font-size" style="font-size:46px"><strong>Com</strong></td>\
                                            <td class="text-center font-size" style="font-size:46px"><strong>Dos</strong></td>\
                                            <td class="text-center font-size" style="font-size:46px"><strong>Un</strong></td>\
                                        </tr>';

                                        foreach($numbers as $number){
                                            $length = strlen($number->number);
                            
                                            $astrics = 4 - $length;
                                
                                            $final_number = $number->number;
                                            for($i=1; $i<($astrics +1); $i++){
                                                $final_number = '*'.$final_number;
                                            }
                                            
                                            $html .='<tr>\
                                            <td class="text-center" width="20%" style="font-size:46px">'. $final_number .'</td>\
                                            <td class="text-center" width="20%" style="font-size:46px">'.(($length > 2 && $number->combinated == 0)? price_format($number->amount) :'*') .'</td>\
                                            <td class="text-center" width="20%" style="font-size:46px">'.(($length > 2 && $number->combinated == 1)? price_format($number->amount) :'*') .'</td>\
                                            <td class="text-center" width="20%" style="font-size:46px">'. (($length == 2)? price_format($number->amount) :'*') .'</td>\
                                            <td class="text-center" width="20%" style="font-size:46px">'. (($length == 1)? price_format($number->amount) :'*') .'</td>\
                                            </tr>';

                                        }  

                                        $html .='<tr>\
                                            <td colspan="5">&nbsp;</td>\
                                        </tr>\
                                        <tr>\
                                            <td colspan="3" width="80%" style="text-align: right; font-size:46px"><strong>Total:</strong></td>\
                                            <td colspan="2" style="text-align: center; font-size:46px">'.price_format($reg->total).'</td>\
                                        </tr></tbody>\
                                    </table>';
   
                                @endphp

                                <div id="inside" class="row"></div>  
                            </div>
                        </div>         

                                @if($reg->printedd == 0 ||  ($rol == 'admin' || $rol == 'manager'))
                                <a class="btn btn-sm btn-outline-main" onclick=" printupdate('{{ $reg->id }}')"><i class="fa fa-print"></i> Imprimir</a>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <a href="{{route('bet.list')}}" class="btn btn-default">Regresar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        var htmlheader = '{!! $html !!}';
        var cssroute  = '{{ asset('css/print.css')}}';
        var update_printed = '{{ route('update_printed') }}'
    </script>
@endsection
@extends('admin.layouts.app')

@section('title', 'Lista de Chanchas')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Chanchas</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">
            <a class="btn btn-outline-main btn-sm" href="{{route('bet.edit',['id'=>0])}}">
                <i class="fa fa-plus-circle"></i> Agregar Chancha
            </a>
            @hasrole('admin')
            <button onclick="filterOrExport('{{route('bet.export')}}')" class="btn btn-outline-main btn-sm"><i
                        class="fa fa-download"
                        aria-hidden="true"></i>
                Exportar a excel
            </button>
            @endhasrole
        </div>
        <!-- Search -->
        <form method="GET" action="" id="bet_form">
            <div class="col-lg-12">
                <br>
                <h2 class="list-title">Búsqueda Avanzada</h2>
            </div>
            <input type="hidden" name="filter" value="1">
            <div class="col-lg-2 form-group  mb-5">
                <label>Fecha</label>
                <input type="date" class="form-control" name="date"
                       value="{{$filters['date'] ?? ''}}"/>
            </div>
            <div class="col-lg-2 form-group  mb-5">
                <label>Lotería</label>
                {!! Form::select('lottery_id', $all_lottery,  $filters['lottery_id'] ?? '', ['class'=>'form-control selectpicker',"placeholder"=>"Todas"]) !!}
            </div>
            @unless('salesman')
            <div class="col-lg-2 form-group mb-5">
                <label>Vendedor</label>
                {!! Form::select('user_id', $users,  $filters['user_id'] ?? '', ['class'=>'form-control selectpicker',"placeholder"=>"Todos"]) !!}

            </div>
            @endunless
            <div class="col-lg-2 form-group mb-5">
                <label>Consecutivo</label>
                <input type="text" class="form-control" name="consecutive"
                       value="{{$filters['consecutive'] ?? ''}}"/>
            </div>
            <div class="col-lg-2 form-group mb-5">
                <label>Número</label>
                <input type="text" class="form-control" name="number"
                       value="{{$filters['number'] ?? ''}}"/>
            </div>
            <div class="col-lg-2 form-group" style="margin-top: 25px">
                <button type="button" onclick="filterOrExport('{{route('bet.list')}}')" class="btn btn-main btn-sm">
                    <i class="fa fa-search"></i> Buscar
                </button>
                <a href="{{route('bet.list')}}" class="btn btn-default btn-sm">
                    <i class="fa fa-trash"></i> Limpiar
                </a>
            </div>
            <div class="col-lg-12">
                <hr>
            </div>
        </form>
        <!-- /Search -->

        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th width="80">#</th>
                    <th>Día Jugado</th>
                    <th>Total</th>
                    <th>Vendedor</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var del_message = '¿Deseas eliminar este Loteria?',
            columns = [
                {"data": "code"},
                {"data": "date"},
                {"data": "total"},
                {"data": "seller"},
                {"data": "options"}
            ];
    </script>
    <script>
        function filterOrExport(action) {
            $('#bet_form').attr('action', action);
            $('#bet_form').submit();
        }
    </script>
@endsection
@extends('admin.layouts.app')

@section('title', 'Lista de sorteos')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Sorteos / Pagos</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">
            <a class="btn btn-outline-main btn-sm" href="javascript:addDraw()">
                <i class="fa fa-plus-circle"></i> Agregar sorteo
            </a>
        </div>
        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Lotería</th>
                    <th>Número ganador</th>
                    <th># Ganadores</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var del_message = '¿Deseas eliminar este sorteo?',
            columns = [
                {"data": "created_at"},
                {"data": "lottery_name"},
                {"data": "number"},
                {"data": "winners_count"},
                {"data": "options"}
            ];
    </script>
@endsection
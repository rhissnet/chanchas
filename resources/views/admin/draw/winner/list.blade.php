@extends('admin.layouts.app')

@section('title')
    Lista de ganadores {{$draw->lottery->name }} {{format_date($draw->created_at)}}
    <span class="badge bg-primary" style="font-size: 16px; letter-spacing: 5px;">{{$draw->number}}</span>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active"><a href="{{route('draws.list')}}">Sorteos / Pagos</a></li>
        <li class="breadcrumb-item active">Ganadores</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">
            <a href="{{route('draws.winners.export',['id'=>$draw->id])}}" class="btn btn-outline-main btn-sm"><i
                        class="fa fa-download"
                        aria-hidden="true"></i>
                Exportar a excel
            </a>
        </div>

        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th width="100">Chancha #</th>
                    <th>Tipo de acierto</th>
                    <th>Número</th>
                    <th>Valor apostado</th>
                    <th>Valor ganado</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var del_message = '¿Deseas eliminar este ganador?',
            control = '{{route('draws.json_winners',['id'=>$draw->id])}}',
            delete_ctrl = '{{route('draws.delete_winner')}}',
            columns = [
                {"data": "bet_id"},
                {"data": "winner_type"},
                {"data": "number"},
                {"data": "amount"},
                {"data": "earned_amount"},
                {"data": "options"}
            ];
    </script>
@endsection
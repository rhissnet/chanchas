@extends('admin.layouts.app')

@section('extracss')

<style>
    .validate-form {
  width: 100%;

}
.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}
.form-label-group > input{
  height: 3.125rem;
  padding: .75rem;
}
i:hover {
  cursor: pointer;
}
</style>
@endsection


@section('title', 'Editar Vendedor')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('sellers.list')}}">Vendedor </a></li>
        <li class="breadcrumb-item active">Editar Vendedor</li>
    </ol>
@endsection

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs cnav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">INFORMACIÓN</a></li>
                </ul>
                <form class="validate-form" method="POST" enctype="multipart/form-data" action="">
                    @csrf
                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">
                                <div class="row">
                                    <div class="col-lg-3 form-group">
                                        <label>Nombre</label>
                                        <input type="text" class="form-control" name="usr[name]"
                                               value="{{$reg->name ?? null}}" required/>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Usuario</label>
                                        <input type="username" class="form-control" name="username"
                                               value="{{$reg->username ?? null}}" required/>
                                        @if ($errors->has('username'))
                                            <small class="invalid-feedback">
                                                {{ $errors->first('username') }}
                                            </small>
                                        @endif
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="usr[email]"
                                               value="{{$reg->email ?? null}}"/>
                                        @if ($errors->has('email'))
                                            <small class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </small>
                                        @endif
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Rol</label>
                                        {{ Form::select('role_id',$rols, ($role_id??1) ,array('class'=>'form-control','placeholder'=>'Selecione el rol', 'required'=>'required'))}}
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-lg-2 form-group">
                                        <label>Código Vendedor</label>
                                        {{ Form::text('prof[code]',$pr->code??null ,array('class'=>'form-control'))}}
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>Estado de la Cuenta</label>
                                        {{ Form::select('usr[active]',['0'=>'Inactiva','1'=>'Activa'],$reg->active??null ,array('class'=>'form-control'))}}
                                    </div>
                                    <div class="col-lg-2 form-group">
                                        <label>Teléfono</label>
                                        {{ Form::text('prof[phone]',$pr->phone??null ,array('class'=>'form-control'))}}
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Dirección</label>
                                        {{ Form::text('prof[address]',$pr->address??null ,array('class'=>'form-control'))}}
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Ciudad</label>
                                        {{ Form::text('prof[city]',$pr->city??null ,array('class'=>'form-control'))}}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label>Observaciones</label>
                                        <textarea class="form-control" rows="4" name="prof[description]">{{ $pr->description ?? null }}</textarea>
                                    </div>
                                </div>



                                <div class="row">
                                    @if($id > 0)
                                    <div class="col-lg-12">
                                        <p>
                                            <label class="md-check">
                                                <input type="checkbox" class="show_pass_check" id="change_password"
                                                        {{ ($errors->has('current_password') || $errors->has('password') || $errors->has('password_confirmation')) ? 'checked' : ''  }}>
                                                <i class="indigo"></i>
                                                Cambiar contraseña
                                            </label>
                                        </p>
                                    </div>
                                    @endif

                                   

                                    <div class="col-lg-4 form-group @if($id > 0)show_pass @endif">
                                        <label>Contraseña</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="password" name="password" @if($id > 0) disabled @endif required>
                                            <span class="input-group-btn">
                                              <button class="btn btn-default" type="button" id="eye_button"><i id="eye" class="fa fa-eye-slash" ></i></button>
                                            </span>
                                          </div>
                            
                                            @if ($errors->has('password'))
                                                <small class="invalid-feedback">
                                                    {{ $errors->first('password') }}
                                                </small>
                                            @endif
                                    </div>

                                    <div class="col-lg-4 form-group @if($id > 0)show_pass @endif">
                                        <label>Repite tu nueva contraseña</label>
                                        <div class="input-group">
                                        <input type="password" class="form-control" id="retype_password"
                                               name="password_confirmation" @if($id > 0) disabled @endif required/>
                                               <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" id="retype_button"><i id="eye_retype" class="fa fa-eye-slash" ></i></button>
                                              </span>
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <small class="invalid-feedback">
                                                {{ $errors->first('password_confirmation') }}
                                            </small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <button type="submit" class="btn btn-main"><i class="fa fa-save"></i> Guardar</button>
                        <a href="{{route('sellers.list')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
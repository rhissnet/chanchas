@extends('admin.layouts.app')


@section('extracss')

<style>
    .validate-form {
  width: 100%;

}
.form-label-group {
  position: relative;
  margin-bottom: 1rem;
}
.form-label-group > input{
  height: 3.125rem;
  padding: .75rem;
}
i:hover {
  cursor: pointer;
}
</style>
@endsection


@section('title', 'Lista de Vendedores')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Vendedores</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">
            <a class="btn btn-outline-main btn-sm" href="{{route('sellers.edit',['id'=>0])}}">
                <i class="fa fa-plus-circle"></i> Agregar Vendedor
            </a>
        </div>

        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Role</th>
                    <th>Estado Cuenta</th>
                   
                    <th>Chanchas Vendidas</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var url_seller  =  '{{ route('sellers.getName') }}';
        var delete_seller = '{{ route('sellers.deleteSeller') }}';
        var del_message = '¿Deseas eliminar este vendedor?',
           
            columns = [
                {"data": "id"},
                {"data": "name"},
                {"data": "user"},
                {"data": "rol"},
                {"data": "active"},
                {"data": "sales"},
                {"data": "options"}
            ];
    </script>
@endsection
@extends('admin.layouts.app')

@section('title', 'Configuración')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Configuración</li>
    </ol>
@endsection

@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs cnav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab"
                                                              data-toggle="tab">INFORMACIÓN APUESTAS</a></li>
                    <!--li role="presentation"><a href="#banners" aria-controls="banners" role="tab"
                                               data-toggle="tab">BANNERS</a></li-->
                </ul>
                <form class="validate-form" method="POST" enctype="multipart/form-data" action="">
                    @csrf
                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">
                                <div class="box-card">
                                    <h2>Configuraciones</h2>
                                    <div class="row">
                                        <div class="col-lg-4 form-group">
                                            <label>Monto acumualdo de Apuestas Número Directo</label>
                                            <input type="text" class="form-control" name="info[max_total_number]"
                                                   value="{{$info['max_total_number'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label>Valor Máximo por Numero Directo</label>
                                            <input type="text" class="form-control" name="info[max_bet]"
                                                   value="{{$info['max_bet'] ?? ''}}"/>
                                        </div>


                                        <div class="col-lg-4 form-group">
                                            <label>Monto acumualdo de Apuestas Número Combinado</label>
                                            <input type="text" class="form-control" name="info[max_total_number_com]"
                                                   value="{{$info['max_total_number_com'] ?? ''}}"/>
                                        </div>

                                        <div class="col-lg-4 form-group">
                                            <label>Valor Máximo por Número Combinado</label>
                                            <input type="text" class="form-control" name="info[max_bet_com]"
                                                   value="{{$info['max_bet_com'] ?? ''}}"/>
                                        </div>

                                        <div class="col-lg-4 form-group">
                                            <label>Valor Minimo de Apuesta</label>
                                            <input type="text" class="form-control" name="info[min_bet]"
                                                   value="{{$info['min_bet'] ?? ''}}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-card">
                                    <h2>Cierre</h2>
                                    <div class="row">
                                        <div class="col-lg-3 form-group">
                                            <label>Hora de Cierre</label>
                                            <input type="text" class="form-control onlyclock" name="info[close_time]"
                                                   value="{{$info['close_time'] ?? ''}}" readonly/>
                                        </div> 
                                        <div class="col-lg-3 form-group">
                                            <label>Días maximo para reclamar Chancha</label>
                                            <input type="text" class="form-control" name="info[reclaim_prizze]"
                                                   value="{{$info['reclaim_prizze'] ?? ''}}" />
                                        </div> 
                                    </div>

                                   
                                </div>

                                <div class="box-card">
                                    <h2>Comisiones</h2>
                                    <div class="row">
                                        <div class="col-lg-3 form-group">
                                            <label>Comisión despues de ventas</label>
                                            <input type="text" class="form-control " name="info[commission_base]"
                                                   value="{{$info['commission_base'] ?? ''}}" />
                                        </div> 

                                        <div class="col-lg-3 form-group">
                                            <label>Comisión Vendedores</label>
                                            <input type="text" class="form-control " name="info[salesman_commission]"
                                                   value="{{$info['salesman_commission'] ?? ''}}" />
                                        </div> 
                                        <div class="col-lg-3 form-group">
                                            <label>Comisión Supervisor</label>
                                            <input type="text" class="form-control " name="info[supervisor_commission]"
                                                   value="{{$info['supervisor_commission'] ?? ''}}" />
                                        </div> 
                                        <div class="col-lg-3 form-group">
                                            <label>Comisión Punto Fijo por Venta</label>
                                            <input type="text" class="form-control " name="info[local_commission]"
                                                   value="{{$info['local_commission'] ?? ''}}" />
                                        </div> 
                                    </div>                                   
                                </div>

                                <!--div class="box-card">
                                    <h2>Redes sociales</h2>
                                    <div class="row">
                                        <div class="col-lg-6 form-group">
                                            <label>Facebook</label>
                                            <input type="text" class="form-control" name="info[facebook]"
                                                   value="{{$info['facebook'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label>Instagram</label>
                                            <input type="text" class="form-control" name="info[instagram]"
                                                   value="{{$info['instagram'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label>Twitter</label>
                                            <input type="text" class="form-control" name="info[twitter]"
                                                   value="{{$info['twitter'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label>Youtube</label>
                                            <input type="text" class="form-control" name="info[youtube]"
                                                   value="{{$info['youtube'] ?? ''}}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-card">
                                    <h2>Información de contacto</h2>
                                    <div class="row">
                                        <div class="col-lg-4 form-group">
                                            <label>Email (contáctenos)</label>
                                            <input type="text" class="form-control" name="info[email]"
                                                   value="{{$info['email'] ?? ''}}" required/>
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label>Teléfono</label>
                                            <input type="text" class="form-control" name="info[phone]"
                                                   value="{{$info['phone'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label>WhatsApp</label>
                                            <input type="text" class="form-control" name="info[whatsapp]"
                                                   value="{{$info['whatsapp'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label>Ciudad</label>
                                            <input type="text" class="form-control" name="info[city]"
                                                   value="{{$info['city'] ?? ''}}"/>
                                        </div>
                                        <div class="col-lg-8 form-group">
                                            <label>Dirección</label>
                                            <input type="text" class="form-control" name="info[address]"
                                                   value="{{$info['address'] ?? ''}}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="box-card">
                                            <h2>Código estadísticas</h2>
                                            <div class="row">
                                                <div class="col-lg-12 form-group">
                                                    <label>Google Analytics (Código de Seguimiento: Ej.
                                                        UA-1234567-12)</label>
                                                    <input type="text" class="form-control" name="info[analytics]"
                                                           value="{{$info['analytics'] ?? ''}}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="box-card">
                                            <h2>Plantilla de correos</h2>
                                            <div class="row">
                                                <div class="col-lg-12 form-group">
                                                    <label>Color de la plantilla</label>
                                                    <input type="text" class="form-control jscolor {hash:true}"
                                                           id="template_color" name="info[template_color]"
                                                           value="{{$info['template_color'] ?? ''}}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-card">
                                            <h2>Ubicación en el mapa</h2>
                                            <div class="row">
                                                <div id="map_canvas" style="width:100%; height:350px"></div>
                                                <input type="hidden" name="info[latitude]" id="latitude_map"
                                                       value="{{$info['latitude'] ?? ''}}">
                                                <input type="hidden" name="info[longitude]" id="longitude_map"
                                                       value="{{$info['longitude'] ?? ''}}">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div-->
              
                    </div>
                    <div class="panel-footer bg-white">
                        <button type="submit" class="btn btn-main"><i class="fa fa-save"></i> Guardar</button>
                        <a href="{{route('dashboard')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var map,
            marker,
            infowindow,
            contentString = '',
            icon = '{{asset('upload/default/marker.png')}}',
            img_default = '{{asset('upload/default/no-image-small.png')}}',
            latitude = '{{get_info('latitude')}}',
            longitude = '{{get_info('longitude')}}',
            icon_map_url = '{{asset('images/marker.png')}}';
    </script>
@endsection
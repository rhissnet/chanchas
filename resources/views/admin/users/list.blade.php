@extends('admin.layouts.app')

@section('title', 'Lista de Usuarios')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Usuarios</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">

    
            <a class="btn btn-outline-main btn-sm" href="{{route('users.edit',['id'=>0])}}">
                <i class="fa fa-plus-circle"></i> Agregar Usuario
            </a>
        </div>

        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Nombre</th>
                    <th>Rol</th>
                    <th>Estado de la Cuenta</th>
                    <th>Creado</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var del_message = '¿Deseas eliminar este Usuarios?',
            columns = [
                {"data": "id"},
                {"data": "name"},
                {"data": "role"},
                {"data": "active"},
                {"data": "created"},
                {"data": "options"}
            ];
    </script>
@endsection
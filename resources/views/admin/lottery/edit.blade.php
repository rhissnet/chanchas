@extends('admin.layouts.app')

@section('title', ($option ?? 'Agregar') . ' Loteria')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('lottery.list')}}">Loterias</a></li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs cnav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab"
                                                              data-toggle="tab">INFORMACIÓN</a></li>
                </ul>
                <form class="validate-form" method="POST" enctype="multipart/form-data" action="">
                    @csrf

                    <div class="panel-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="info">
                                <div class="row">
                                    <div class="col-lg-3 form-group">
                                        <label>Nombre de la Chancha </label>
                                        <input type="text" class="form-control" name="dat[name]"
                                               value="{{$reg->name ?? null}}" required
                                               maxlength="200"/>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Abreviación de la Chancha </label>
                                        <input type="text" class="form-control" name="dat[abv]"
                                               value="{{$reg->abv ?? null}}"
                                               maxlength="4"/>
                                    </div>
                                    <div class="col-lg-3 form-group">
                                        <label>Día de la Semana</label>
                                        {{ Form::select('dat[weekday]', config('arrays.weekdays') ,$reg->weekday??null,array('class'=>'form-control')) }}
                                    </div>

                                    <div class="col-lg-3 form-group">
                                        <label>Estado</label>
                                        {{ Form::select('dat[status]', config('arrays.status') ,$reg->status??null,array('class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 form-group"><hr/></div>
                                </div>
                                @if(($reg->id ?? null) > 0)
                                <div class="row">
                                    <div class="col-lg-3 form-group"></div>

                                    <div class="col-lg-4 form-group">
                                    <div class="list_table_block">
                                        <a href="#" onclick="addblock('{{ $reg->id }}')"  class="btn btn-outline-main btn-sm form-control"><i class="fa fa-plus-circle"></i> Agregar Numero</a>
                                        <table class="table table-striped b-t b-b" id="datatable" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Numero</th>
                                                <th width="200">Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bnumbers">
                                                @foreach($numbers as $out)
                                                    <tr id="numberspace{{ $out->id }}">
                                                    <td>{{ $out->number }}</td>
                                                    <td><a class="btn btn-sm btn-outline-danger" href="javascript:deletenumber(' {{ $out->id }} ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>
                                   
                                    <div class="col-lg-2 form-group">
                                        <label>&nbsp; </label>
                                        
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer bg-white">
                        <button type="submit" class="btn btn-main"><i class="fa fa-save"></i> Guardar</button>
                        <a href="{{route('lottery.list')}}" class="btn btn-default">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script>
        var save_block_number = '{{ route('lottery.save_block') }}';
        var del_message_number = 'Esta seguro de Eliminar el numero?';
        var del_url            = '{{ route('loterry.delete_number') }}';
    </script>
@endsection
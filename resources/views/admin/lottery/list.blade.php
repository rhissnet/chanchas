@extends('admin.layouts.app')

@section('title', 'Lista de Loterias')

@section('breadcrumb')
    <ol class="breadcrumb bread-main">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="mdi-action-home"></i></a></li>
        <li class="breadcrumb-item active">Loterias</li>
    </ol>
@endsection

@section('body')
    @include('admin.includes.modals')
    <div class="panel panel-default">
        <div class="panel-heading heading-actions">

            <a class="btn btn-outline-main btn-sm" href="{{route('lottery.edit',['id'=>0])}}">
                <i class="fa fa-plus-circle"></i> Agregar Loteria
            </a>
        </div>

        <div class="list_table">
            <table class="table table-striped b-t b-b" id="datatable" width="100%">
                <thead>
                <tr>
                    <th width="10">#</th>
                    <th>Nombre</th>
                    <th>Dia de la Semana</th>
                    <th>Estado</th>
                    <th>Numeros Bloqueados</th>
                    <th width="200">Opciones</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js_vars')
    <script>
        var del_message = '¿Deseas eliminar este Loteria?',
            columns = [
                {"data": "id"},
                {"data": "name"},
                {"data": "weekday"},
                {"data": "status"},
                {"data": "number"},
                {"data": "options"}
            ];
    </script>
@endsection
$(function () {
    if ($("#traffic_table").length > 0) {
        loadChart();
    }

    if($("#sales_report_table").length > 0){
        loadSalesChart();
    }

});

function loadSalesChart(){
    
    var date_start = $("#date_start").val();
    var date_end = $("#date_end").val();
    var salesman = $("#salesman").val();

    $(".btn-load").prop('disabled', true);
    $(".btn-load").html('<i class="fa fa-spinner" aria-hidden="true"></i> Cargando estadísticas...');

    $.get('/cms/dashboard/sales', {date_start: date_start, date_end: date_end,salesman:salesman}, function (info) {
        var fechai = date_start.split("-");

        $('#visits').html(info.tiempo);
        $('#time').text(info.cantidad);

        Highcharts.chart('stats', {
            title: {
                text: 'Ventas de Chanchas Diarías'
            },
            xAxis: {
                type: 'datetime',
                title: {
                    text: 'Días'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Ventas Chanchas'
                }
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            series: [{
                name: info.name,
                data: info.data
            }],
            series: [{
                name: 'Cantidad de Chanchas',
                data: info.data
            }, {
                name: 'Ventas en Soles',
                data: info.money
            }],
    
            plotOptions: {
                line: {
                    cursor: 'pointer',
                    pointInterval: 24 * 60 * 60 * 1000,
                    pointStart: Date.UTC(fechai[0], fechai[1] - 1, fechai[2], 0, 0, 0)

                }
            },
        });
    });


    $.get('/cms/reports/sales-general', {date_start: date_start, date_end: date_end}, function (info) {
        $('#lottery_table').html(info.lottery);
        $('#number_table').html(info.numbers);
        $('#other_table').html(info.others);
    });


    $(".btn-load").prop('disabled', false);
    $(".btn-load").html('<i class="fa fa-line-chart" aria-hidden="true"></i> Cargar Estadísticas');

}

function loadChart() {
    var date_start = $("#date_start").val();
    var date_end = $("#date_end").val();
    var salesman = $("#salesman").val();

    $(".btn-load").prop('disabled', true);
    $(".btn-load").html('<i class="fa fa-spinner" aria-hidden="true"></i> Cargando estadísticas...');

    $.get('/cms/dashboard/sales', {date_start: date_start, date_end: date_end,salesman:salesman}, function (info) {
        var fechai = date_start.split("-");

        $('#visits').html(info.tiempo);
        $('#time').text(info.cantidad);

        Highcharts.chart('stats', {
            title: {
                text: 'Ventas de Chanchas Diarías'
            },
            xAxis: {
                type: 'datetime',
                title: {
                    text: 'Días'
                }
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Ventas Chanchas'
                }
            },
            subtitle: {
                text: ''
            },
            legend: {
                enabled: false
            },
            series: [{
                name: info.name,
                data: info.data
            }],
            series: [{
                name: 'Cantidad de Chanchas',
                data: info.data
            }, {
                name: 'Ventas en Soles',
                data: info.money
            }],
    
            plotOptions: {
                line: {
                    cursor: 'pointer',
                    pointInterval: 24 * 60 * 60 * 1000,
                    pointStart: Date.UTC(fechai[0], fechai[1] - 1, fechai[2], 0, 0, 0)

                }
            },
        });
    });


    $.get('/cms/dashboard/general', {date_start: date_start, date_end: date_end,salesman:salesman}, function (info) {
        $('#lottery_table').html(info.lottery);
        $('#number_table').html(info.numbers);
    });


    $(".btn-load").prop('disabled', false);
    $(".btn-load").html('<i class="fa fa-line-chart" aria-hidden="true"></i> Cargar Estadísticas');
}

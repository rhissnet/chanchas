$("#form_add_draw").submit(function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.find('button').prop("disabled", true);
    var draw_number = document.getElementById('draw_number').value;
    if (draw_number.length < 4 || draw_number.length > 4) {
        showMessage('error', 'El número debe tener 4 dígitos.');
        $this.find('button').prop("disabled", false);
    }else{
        $("#form_add_draw")[0].submit();
    }
});

function addDraw() {
    $('#form_add_draw').trigger("reset");
    $("#modal_draw").modal('show');
}
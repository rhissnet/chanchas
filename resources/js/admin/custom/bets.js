$(function () {

    if ($('.bet-form').length) {
        var disable_days = [7];
        var disable_dates = ['2021-12-10'];
        $('#date_chancha').bootstrapMaterialDatePicker({
            time: false,
            format: 'YYYY-MM-DD',
            clearButton: false,
            switchOnClick: true,
            disabledDays: disable_days,
            disabledDates: disable_dates,
            minDate: moment(),
            maxDate: moment().add(1, 'days'),
            cancelText: 'Cancelar',
            okText: 'Aceptar',
            nowText: 'Hoy',
            lang: 'es'
        })
    }


    $("#space1").inputFilter(function (value) {
        return /^-?\d*$/.test(value);
    });


});

$("#form_number_bet").submit(function (e) {
    e.preventDefault();
    var number      = $('#space1').val();
    var combinated  = $('#combianted').val();
    var value       = $('#bet_value').priceToFloat();

    var total_played = 0;

    lottery_id_ = $('#modale_loterry').val();
   
    for(i=1; i<start; i++){

        id_lot            = parent.$('#lottery_id'+i).val();
        acutal_number     = parent.$('#number'+i).val();
        acutal_combinated = parent.$('#combinated'+i).val();

        if(lottery_id_ == id_lot && number == acutal_number && combinated == acutal_combinated){

            amount_played = parent.$('#ammount'+i).val();
            total_played = parseFloat(amount_played) + parseFloat(total_played);
        }

    }

    $('#real_value').val(value);
    var data = $('#form_number_bet').serialize()+ "&ammountplayed=" + total_played+ "&loyerry_id=" + lottery_id_;

    $.post(validatenumerUrl, data, function (response) {
        if (response.status === 'error') {
            showMessage('error', response.msg);
            return;
        }

        var extra = '';

        if(combinated == 1){
            var extra = 'C';
        }

        var loterry_id = $('#modale_loterry').val();
        total_length = loterry_id.length;

            var duplicate = 1;
            if(total_length > 2){
                    duplicate =2;
            }

        var parcial = $('#total_final').val();
        var total = parseFloat(parcial) + (parseFloat(value) * duplicate);
        total = total.toFixed(2);
        $('#total_final').val(total);
        $('#total_text').text(total);

        $('#list_numbers').append(' <li class="list-group-item" id="block' + start + '">\
        <span class="pull-right text-muted m-l-xs" onclick="removeblock('+start+')"><i class="glyphicon glyphicon-remove"></i></span>\
        <span class="pull-right badge badge-lg bg-success">' + $('#bet_value').val() + '</span>\
        <div class="block font-bold">' + number + '<strong> ' + extra + ' </strong></div>\
        ' + $("#modale_loterry option:selected").text() + '</li>');


        $('.bet-form').append('<input value="' + $("#modale_loterry").val() + '" type="hidden" name="bet[lottery_id][]" id="lottery_id' + start + '">\
                <input value="' + value + '" name="bet[ammount][]" type="hidden" id="ammount' + start + '">\
                <input value="' + number + '" name="bet[number][]" type="hidden" id="number' + start + '">\
                <input value="' + combinated + '" name="bet[combinated][]" type="hidden" id="combinated' + start + '">')

        $('#date_chancha').prop('disabled', true);
        $("#modal_chanchas").modal('hide');

        start++;
        realnumber++;

        blockbutton();
    });
});

$("#form_bet").submit(function (e) {
    e.preventDefault();


    var date = $('#date_chancha').val();

    if (min_bet > 0 && $('#total_final').val() < min_bet) {
        showMessage('error', 'El monto mínimo de apuesta es: ' + min_bet);
    } else if (isClosingTime(date)) {

        showMessage('error', 'Se han cerrado las apuestas del día: ' + date);
    } else {

        
        $('#bet_date').val(date);
        $("#form_bet")[0].submit();
    }
});

function addZero(i) {
    if (i < 10) {i = "0" + i}
    return i;
  }

function isClosingTime(bet_date) {
    var today = new Date();
    var parts = bet_date.split('-');
    var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
    var time = addZero(today.getHours()) + ":" + addZero(today.getMinutes());
    today.setHours(12,0,0,0);
    mydate.setHours(12,0,0,0);


    return (today.getTime() === mydate.getTime()) && (time >= close_time);
}



function openBetModal() {

    if(realnumber > 6){
        confirmModal({
            messageText: 'A completado la cantidad de numeros permitidos jugar por chancha',
            alertType: "danger"
        }).done(function (e) {

        })
    }else{

        $('#form_number_bet').trigger("reset");

        activatedcombinated();

        var getdate = $('#date_chancha').val();

        $('#date_hidden').val(getdate);

        $.post(get_loterry_url, {_token: token_src, getdate: getdate}, function (data) {

            $('#modale_loterry').html($('<option>', {value: '', text: 'Seleccione Loteria'}));
            $('#modale_loterry').append(data);


            for(i=1; i<start; i++){

                id_lot = parent.$('#lottery_id'+i).val();
                $('#modale_loterry').val(id_lot);
                $('#modale_loterry').prop('disabled', true);
            }

        });

        $("#modal_chanchas").modal('show');
    }
}

function removeblock(id) {


    confirmModal({
        messageText: 'Esta seguro de eliminar este numero?',
        alertType: "danger"
    }).done(function (e) {
            var amount = $('#ammount' +id).val();
            var total  = $('#total_final').val();

            var loterry = $('#lottery_id' + id).val();

            total_length = loterry.length;

            var duplicate = 1;
            if(total_length > 2){
                    duplicate =2;
            }

            newtotal = parseFloat(total) - (parseFloat(amount) * duplicate);

            if(newtotal == 0){
                $('#modale_loterry').prop('disabled', false);
            }

            $('#total_final').val(newtotal);
            $('#total_text').text(newtotal);

            $('#block'+id).remove();

            realnumber --;
            blockbutton();
    });

}


function activatedcombinated(){

    number = $('#space1').val();
    numlength = number.length;

    if(numlength > 2 ){
        $('#combianted').removeAttr('disabled');
    }else{
        $('#combianted').val(0);
        $('#combianted').prop('disabled', true);
    }

}


function addblock(id) {

    $('#form_add_block').trigger("reset");
    $('#lottery_id').val(id);
    $("#modal_block_number").modal('show');

}

function addblock_number(id) {

    data = $('#form_add_block').serialize();

    $.post(save_block_number, data, function (response) {

        $('#bnumbers').append('<tr id="numberspace' + response.id + '">\
        <td>' + $('.bnumber_form').val() + '</td>\
        <td><a class="btn btn-sm btn-outline-danger" href="javascript:deletenumber(' + response.id + ')"><i class="fa fa-trash" aria-hidden="true"></i> Eliminar</a></td>\
        </tr>');

        $("#modal_block_number").modal('hide');
    })

}

function deletenumber(id) {

    confirmModal({
        messageText: del_message_number,
        alertType: "danger"
    }).done(function (e) {
        $('#numberspace' + id).hide();
        $.get(del_url + '/' + id, function (data) {
            var msj = 'Ha ocurrido un error';
            var type = 'error';
            if (data === '1') {
                msj = 'Registro eliminado';
                type = 'success';
            }
            showMessage(type, msj);
        })
    });

}

function printupdate(id){

    $.post(update_printed, {_token: token_src, id: id}, function (data) {
        printJS({
            printable: 'inside', 
            type: 'html',
            header: htmlheader,
            css: cssroute,
            maxWidth:'900',
            scanStyles:true
        })
    });
        
}

function blockbutton(){
        $('#addnumb').removeAttr('disabled');
}